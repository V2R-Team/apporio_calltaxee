<?php
error_reporting(0);
include_once '../apporioconfig/start_up.php';
header("Content-Type: application/json");
$user_id = $_REQUEST['user_id'];

if ($user_id != "" ) {
    $query3 = "select * from favourites_destinations where user_id='$user_id'";
    $result3 = $db->query($query3);
    $ex_rows = $result3->num_rows;
    if ($ex_rows != 0) {
        $list = $result3->rows;
        $re = array('result' => 1, 'msg' => "Favourite Location Added",'details'=>$list);
    } else {
        $re = array('result' => 0, 'msg' => "No Favourite Location");
    }
} else {
    $re = array('result' => 0, 'msg' => "Required Field Missing");
}
echo json_encode($re, JSON_PRETTY_PRINT);
?>