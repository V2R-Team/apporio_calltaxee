<?php
class Ride extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Commonmodel');
    }

    function Track($url){
        $this->load->library('encrypt');
        $enc_ride_id =str_replace(array('-', '_', '~'), array('+', '/', '='), $url);
        $ride_id=$this->encrypt->decode($enc_ride_id);
        $data = $this->Commonmodel->ride_details($ride_id);
        $ride_status = $data->ride_status;
        switch ($ride_status){
            case "1":
                echo '<h1>Ride Not Started</h1>';
                break;
            case "2":
                echo '<h1>Ride Cancelled By User</h1>';
                break;
            case "3":
                $this->load->view('map',['data'=>$data]);
                break;
            case "4":
                echo '<h1>Ride Cancelled By Driver</h1>';
                break;
            case "5":
                $this->load->view('map',['data'=>$data]);
                break;
            case "6":
                $this->load->view('map',['data'=>$data]);
                break;
            case "7":
                echo '<h1>Ride Completed</h1>';
                break;
            case "8":
                echo '<h1>Ride Not Started</h1>';
                break;
            case "17":
                echo '<h1>Trip Cancel By Admin</h1>';
                break;
            default:
                echo '<h1>No Record Found</h1>';
        }

    }
}