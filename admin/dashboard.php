<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
function sortByOrder($a, $b)
{
    return $a['distance'] - $b['distance'];
}
$query = "select * from admin_panel_settings WHERE admin_panel_setting_id=1";
$result = $db->query($query);
$admin_settings = $result->row;
$admin_panel_firebase_id = $admin_settings['admin_panel_firebase_id'];
$admin_panel_rideus_request = $admin_settings['admin_panel_rideus_request'];
$admin_panel_request = $admin_settings['admin_panel_request'];
include('common.php');
require 'pn_android.php';
require 'pn_iphone.php';
$query="select * from driver where  current_lat !='' and current_long !=''";
$result = $db->query($query);
$list=$result->rows;
foreach($list as $driver) {
    $driver_id = $driver['driver_id'];
    $driver_name = $driver['driver_name'];
    $current_lat = $driver['current_lat'];
    $current_long = $driver['current_long'];
    $busy = $driver['busy'];
    $driver_status =  "On Ride";
    $image = ICON2_URL;
    if ($busy == 0)
    {
        $online_offline = $driver['online_offline'];
        if ($online_offline != 1)
        {
            $driver_status =  "Offline";
            $image = ICON1_URL;
        }else{
            $driver_status =  "Online";
            $image = ICON_URL;
        }
    }

    $all_driver[] = array(
        'driver_id' =>$driver_id,
        'driver_name'=>$driver_name,
        'current_lat'=>$current_lat,
        'current_long'=>$current_long,
        'driver_status'=>$driver_status,
        'map_icon'=>$image
    );
}
$query="select * from car_type";
$result = $db->query($query);
$list123 = $result->rows;

$query = "select * from country";
$result = $db->query($query);
$country = $result->rows;
$query = "select * from coupons where start_date <= CURDATE() AND expiry_date >= CURDATE()";
$result = $db->query($query);
$coupons = $result->rows;
if(isset($_POST['save'])) {
    $user_id = $_POST['user_id'];
    $phonecode = $_POST['country'];
    $userphone = $_POST['userphone'];
    $phone = $phonecode.$userphone;
    $username = $_POST['username'];
    $email = $_POST['email'];
    $origin = $_POST['origin-input'];
    $destination = $_POST['destination-input'];
    $car_type = $_POST['car_type'];
    $driver = $_POST['driver'];
    $ride_now = $_POST['ride_now'];

    //pickup_location lat and long

    $address = str_replace(" ", "+", $origin);
    $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response);
    $lat = $response_a->results[0]->geometry->location->lat;
    $long = $response_a->results[0]->geometry->location->lng;

    $address1 = str_replace(" ", "+", $destination);
    $url1 = "http://maps.google.com/maps/api/geocode/json?address=$address1&sensor=false";
    $ch1 = curl_init();
    curl_setopt($ch1, CURLOPT_URL, $url1);
    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch1, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
    $response1 = curl_exec($ch1);
    curl_close($ch1);
    $response_a1 = json_decode($response1);
    $lat1 = $response_a1->results[0]->geometry->location->lat;
    $long1 = $response_a1->results[0]->geometry->location->lng;

    $datepicker = $_POST['datepicker'];
    $timepicker = $_POST['timepicker'];
    $coupon_code = $_POST['coupon_code'];
    $dt = DateTime::createFromFormat('!d/m/Y', date("d/m/Y"));
    $data=$dt->format('M j');
    $day=date("l");
    $date=$day.", ".$data ;
    $time=date("h:i A");
    $date1=date("Y-m-d");
    $last_time_stamp = date("h:i:s A");
    if (empty($user_id))
    {
        $data = "select user_id from user WHERE user_phone=$phone";
        $result = $db->query($data);
        $list = $result->row;
        if (!empty($list))
        {
            $user_id = $list['user_id'];
        }else{
            $query2="INSERT INTO user (user_phone,user_name,user_email) VALUES ('$phone','$username','$email')";
            $db->query($query2);
            $user_id = $db->getLastId();
        }
    }
    $query2="INSERT INTO ride_table (ride_platform,coupon_code,date,later_date,later_time,user_id,car_type_id,pickup_lat,pickup_long,pickup_location,drop_lat,drop_long,drop_location,ride_date,ride_time,ride_type,ride_status,ride_admin_status,last_time_stamp) 
         VALUES ('2','$coupon_code','$date1','$datepicker','$timepicker','$user_id','$car_type','$lat','$long','$origin','$lat1','$long1','$destination','$date','$time','$ride_now',1,1,'$last_time_stamp')";
    $db->query($query2);
    $ride_id = $db->getLastId();
    $query5="INSERT INTO table_user_rides(booking_id,ride_mode,user_id) VALUES ('$ride_id','1','$user_id')";
    $db->query($query5);
    if ($ride_now == 1)
    {
        if ($driver == "")
        {
            $query = "select * from driver where online_offline = 1 and car_type_id='$car_type' and driver_admin_status=1 and busy=0 and login_logout=1";
            $result = $db->query($query);
            $list = $result->rows;
            $c = array();
            foreach($list as $login)
            {
                $driver_lat = $login['current_lat'];
                $driver_long = $login['current_long'];
                $theta = $long - $driver_long;
                $dist = sin(deg2rad($lat)) * sin(deg2rad($driver_lat)) +  cos(deg2rad($lat)) * cos(deg2rad($driver_lat)) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles1 = $dist * 60 * 1.1515;
                $km=$miles1* 1.609344;
                if($km <= $admin_panel_rideus_request)
                {
                    $c[] = array("driver_id"=> $login['driver_id'],"distance" => $km);
                }
            }

            if(!empty($c))
            {
                $nodes = array();
                if ($admin_panel_request == 1)
                {
                    foreach($c as $driver){
                        $driver_id = $driver['driver_id'];
                        $ride_id = (string)$ride_id;
                        $nodes[$driver_id] = array('ride_id'=>$ride_id,'ride_status'=>"1");
                        $query3="select * from driver_ride_allocated where driver_id='$driver_id'";
                        $result3 = $db->query($query3);
                        $driver_allocated = $result3->row;
                        if (empty($driver_allocated))
                        {
                            $query5="INSERT INTO driver_ride_allocated (driver_id,ride_id,ride_mode) VALUES ('$driver_id','$ride_id','1')";
                            $db->query($query5);
                        }else{
                            $query5="UPDATE driver_ride_allocated SET ride_id='$ride_id' WHERE driver_id='$driver_id'" ;
                            $db->query($query5);
                        }
                        $query5="INSERT INTO ride_allocated (allocated_ride_id, allocated_driver_id,allocated_date) VALUES ('$ride_id','$driver_id','$date')";
                        $db->query($query5);
                    }
                    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/.json';
                    $ch = curl_init();
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                    curl_setopt($ch,CURLOPT_POST, count($nodes));
                    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($nodes));
                    $response = curl_exec($ch);
                    foreach ($c as $driver){
                        $driver_id = $driver['driver_id'];
                        $query4="select * from driver where driver_id='$driver_id'";
                        $result4 = $db->query($query4);
                        $list4=$result4->row;
                        $device_id=$list4['device_id'];
                        $language="select * from messages where language_id=1 and message_id=32";
                        $lang_result = $db->query($language);
                        $lang_list=$lang_result->row;
                        $message=$lang_list['message_name'];
                        $ride_id= (String) $ride_id;
                        $ride_status= '1';
                        if($device_id!="")
                        {
                            if($list4['flag'] == 1)
                            {
                                IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                            }
                            else
                            {
                                AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                            }
                        }
                    }
                }else{
                    foreach ($c as $value)
                    {
                        $distance[] = $value['distance'];
                    }
                    array_multisort($distance,SORT_ASC,$c);
                    $driver_id = $c[0]['driver_id'];
                    $nodes[$driver_id] = array('ride_id'=>$ride_id,'ride_status'=>"1");
                    $query3="select * from driver_ride_allocated where driver_id='$driver_id'";
                    $result3 = $db->query($query3);
                    $driver_allocated = $result3->row;
                    if (empty($driver_allocated))
                    {
                        $query5="INSERT INTO driver_ride_allocated (driver_id,ride_id,ride_mode) VALUES ('$driver_id','$last_id','1')";
                        $db->query($query5);
                    }else{
                        $query5="UPDATE driver_ride_allocated SET ride_id='$last_id' WHERE driver_id='$driver_id'" ;
                        $db->query($query5);
                    }
                    $query5="INSERT INTO ride_allocated (allocated_ride_id, allocated_driver_id,allocated_date) VALUES ('$last_id','$driver_id','$date')";
                    $db->query($query5);
                    $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/.json';
                    $ch = curl_init();
                    curl_setopt($ch,CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                    curl_setopt($ch,CURLOPT_POST, count($nodes));
                    curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($nodes));
                    $response = curl_exec($ch);
                    $query4="select * from driver where driver_id='$driver_id'";
                    $result4 = $db->query($query4);
                    $list4=$result4->row;
                    $device_id=$list4['device_id'];
                    $language="select * from messages where language_id=1 and message_id=32";
                    $lang_result = $db->query($language);
                    $lang_list=$lang_result->row;
                    $message=$lang_list['message_name'];
                    $ride_id= (String) $ride_id;
                    $ride_status= '1';
                    if($device_id!="")
                    {
                        if($list4['flag'] == 1)
                        {
                            IphonePushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                        }
                        else
                        {
                            AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                        }
                    }
                }

                $db->redirect("home.php?pages=ride-now");
            }else{
                $sql2="UPDATE ride_table SET ride_status = 1 WHERE ride_id ='$ride_id'";
                $db->query($sql2);
                $db->redirect("home.php?pages=ride-now");
            }
        }else{
            $query = "select * from driver where driver_id='$driver'";
            $result = $db->query($query);
            $list = $result->row;
            $device_id = $list['device_id'];
            $booking_status = 1;
            $query5="INSERT INTO ride_allocated (allocated_ride_id,allocated_driver_id,allocated_ride_status) VALUES ('$ride_id','$driver','$booking_status')";
            $db->query($query5);
            $sql2="UPDATE ride_table SET ride_status = 1 WHERE ride_id ='$ride_id'";
            $db->query($sql2);
            $message = "New Ride Allocated";
            $ride_id= (String) $ride_id;
            $ride_status= (String) 1;
            if($device_id  != "")
            {
                if($list['flag'] == 1)
                {
                    IphonePushNotificationDriver($device_id,$message,$ride_id,$ride_status);
                }
                else
                {
                    AndroidPushNotificationDriver($device_id, $message,$ride_id,$ride_status);
                }
            }
            $url = 'https://'.$admin_panel_firebase_id.'.firebaseio.com/Activeride/'.$driver.'/.json';
            $fields = array(
                'ride_id' => (string)$ride_id,
                'ride_status'=>"1",
            );

            $ch = curl_init();
            curl_setopt($ch,CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch,CURLOPT_POST, count($fields));
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
            $response = curl_exec($ch);
            $db->redirect("home.php?pages=ride-now");
        }
    }
    $db->redirect("home.php?pages=ride-now");

}

$query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id WHERE ride_status IN (1,3,5,6) ORDER BY ride_table.ride_id DESC";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $key=>$value)
{
    $driver_id = $value['driver_id'];
    $payment_option_id = $value['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];

    if($driver_id == 0)
    {
        $driver_name = "";
        $driver_email = "";
        $driver_phone = "";
    }else{
        $query1 = "select * from driver where driver_id ='$driver_id'";
        $result1 = $db->query($query1);
        $list1 = $result1->row;
        $driver_name = $list1['driver_name'];
        $driver_email = $list1['driver_email'];
        $driver_phone = $list1['driver_phone'];
    }
    $list[$key]=$value;
    $list[$key]["driver_name"] = $driver_name;
    $list[$key]["driver_email"] = $driver_email;
    $list[$key]["driver_phone"] = $driver_phone;
    $list[$key]["payment_option_name"]=$payment_option_name;
}
?>
<style xmlns="http://www.w3.org/1999/html">
    #map {
        height: 350px;
        width: 100%;;
        position: relative;
        overflow: hidden;
    }
    .clear{ clear: both !important;}

</style>

<link href="css/calander.css" rel="stylesheet" />
<script src="js/calander_jquery.js"></script>
<script src="js/calander_jquery-ui.js"></script>
<script src="js/wickedpicker.js"></script>
<link href="css/wickedpicker.css" rel="stylesheet" />
<script>
    var j = jQuery.noConflict();
    j(document).ready(function() {
        j("#datepicker").datepicker({ dateFormat: 'yy-mm-dd', minDate: 0 }).attr('readOnly', 'true');
        j('#timepicker').wickedpicker({twentyFour: true,title: 'Select Time'});
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#get_details').on('click', function () {
            var country = $('#country').val();
            var phone = $('#userphone').val();
            var userphone = country+phone;
            if(phone == "")
            {
                alert("Enter Phone Number");
                return false;
            }else{
                $.ajax({
                    type: "POST",
                    url: 'serach_user.php',
                    data: 'phone=' + userphone,
                    dataType: 'json',
                    success: function (data)
                    {
                        if (data != "") {
                            $('#user_id').val(data.user_id);
                            $('#username').val(data.user_name);
                            $('#email').val(data.user_email);
                        }else {
                            $("#username").val('');
                            $('#email').val('');
                            $('#user_id').val('');
                            alert("This Phone Number Is Not Registered")
                        }
                    }
                });
            }


        });
    });
    function getId(val) {
        $.ajax({
            type: "POST",
            url: "serach_driver2.php",
            data: "car_type="+val,
            success:
                function(data){
                    $('#driver').html(data);
                }
        });
    }

    function disableMyText(){
        if(document.getElementById("checkMe").checked == true){
            document.getElementById("driver").disabled = true;
        }else{
            document.getElementById("driver").disabled = false;
        }
    }

    function setId(val) {
        var type = val;
        if(type == 1)
        {
            document.getElementById("datepicker").disabled = true;
            document.getElementById("timepicker").disabled = true;
        }else{
            document.getElementById("datepicker").disabled = false;
            document.getElementById("timepicker").disabled = false;
        }
    }


    function validatelogin() {
        var userphone = document.getElementById('userphone').value;
        var username = document.getElementById('username').value;
        var origin = document.getElementById('origin-input').value;
        var destination = document.getElementById('destination-input').value;
        var car_type = document.getElementById('car_type').value;
        var ride_now = document.getElementById('ride_now').value;
        if(userphone == ""){ alert("Enter Phone Number"); return false; }
        if(username == ""){ alert("Enter Rider Name"); return false; }
        if(ride_now == ""){ alert("Select Ride Type");return false;}
        if(origin == ""){ alert("Enter Pickup Location"); return false; }
        if(destination == ""){ alert("Enter Drop Up Location"); return false; }
        if(car_type == ""){ alert("Select Car type"); return false; }
        if(document.getElementById('checkMe').checked == false && document.getElementById('driver').value == ""){
            alert("Select Driver Assign Type");
            return false;
        }
        if(ride_now == 2 && document.getElementById('datepicker').value == "")
        {
            alert("Select Ride Date");
        }
        if(ride_now == 2  && document.getElementById('timepicker').value == "")
        {
            alert("Select Ride Time");
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQMuEgUYbsZCfCzAkm2ogL0hmLfpb0i0Q&v=3.exp&libraries=places"></script>


<style>
    body{
        zoom:100% !important;
    }
</style>
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Manual Taxi Dispatch</h3>
        <select id="type" onchange="filterMarkers(this.value);" style="height: 50px; width: 177px;float: right;">
            <option value="">All Driver</option>
            <option value="Online">Online</option>
            <option value="Offline">Offline</option>
            <option value="On Ride">On Ride</option>
        </select>
        <nav id="sidebar-wrapper" class="">
            <div id="driver_details">
                <ul class="sidebar-nav ">
                    <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i style="color:#ffffff;" class="fa fa-times"></i></a>
                    <li style="clear:both;"></li>

                </ul>
            </div>
        </nav>

    </div>
    <div class="panel-body">
        <form method="post" enctype="multipart/form-data"  onSubmit="return validatelogin()">
            <div class="col-sm-5">

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                    <select class="form-control" style="height: 28px" id="country" name="country">
                        <?php foreach($country as $country_list){ ?>
                            <option value="<?php echo $country_list['phonecode'];?>" <?php if($country_list['phonecode'] == "+91"){ ?> selected <?php } ?>><?php echo $country_list['name']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                    <input class="form-control" id="userphone" name="userphone" style="height:30px" placeholder="Enter Phone Number" type="text">
                    <input type="hidden" id="user_id" name="user_id" value="">
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                    <button type="button" id="get_details" style="height: 30px" class="btn btn-success col-md-12">Get Details</button>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                    <input class="form-control" id="username" name="username" placeholder="Username" style="height: 30px" type="text">
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                    <input class="form-control" id="email" placeholder="Email" style="height: 30px" name="email" type="email">
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                    <select class="form-control" id="ride_now" style="height: 30px" name="ride_now" onchange="setId(this.value);">
                        <option value="1">Ride Now</option>
                        <option value="2">Ride Later</option>
                    </select>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input class="form-control" id="datepicker" name="datepicker" style="height: 30px" placeholder="Select Date" type="text" disabled>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input class="form-control" id="timepicker" name="timepicker" style="height: 30px" placeholder="Select Time" type="text" disabled>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input class="form-control" id="origin-input" name="origin-input" style="height: 30px" placeholder="Pick up Location" type="text">
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <input class="form-control" id="destination-input" name="destination-input" style="height: 30px" placeholder="Drop off Location" type="text">
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <select class="form-control" id="car_type" name="car_type" style="height: 30px" onchange="getId(this.value);">
                        <option value="">Select Vehicle type</option>
                        <?php foreach($list123 as $cartype){ ?>
                            <option value="<?php echo $cartype['car_type_id'];?>"><?php echo $cartype['car_type_name']; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <select class="form-control" name="coupon_code" id="coupon_code" style="height: 30px">
                        <option value="">Apply Coupon</option>
                        <?php foreach ($coupons as $list_coupan):?>
                            <option value="<?php echo $list_coupan['coupons_code']; ?>"><?php echo $list_coupan['coupons_code']."(".$list_coupan['coupons_price'].")"; ?></option>
                        <?php endforeach;?>
                    </select>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-12 form-group has-feedback">
                    <div class="checkbox">
                        <label>
                            <input value="1" type="checkbox" id="checkMe" name="checkMe" onclick="disableMyText()" checked="yes"> Auto Assign Driver
                        </label>

                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
                    <h4><b>OR</b></h4>
                </div>


                <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                    <select class="form-control" name="driver" style="height: 30px" id="driver" disabled>
                        <option value="">Select Driver</option>
                    </select>
                </div>

                <div class="clearfix"></div><br>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <input type="submit" class="btn btn-success" id="save" style="height: 30px" name="save" value="Book Now" >
                </div>
                <div class="clear"></div>

            </div>

        </form>
        <div class="col-sm-7">
            <div class="col-sm-12">
                <div class="row">

                    <div style="zoom: 100%" id="map">
                    </div>
                </div>
            </div>
        </div>



    </div>
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 mobtbl" style="height: 300px">
                    <table id="datatable" class="table table-striped table-bordered table-responsive">
                        <thead>
                        <tr>
                            <th width="5">Sr.No</th>
                            <th width="5">Ride Id</th>
                            <th width="5">Rider Details</th>
                            <th width="5">Driver Details</th>
                            <th>Pickup Address</th>
                            <th>Drop Address</th>
                            <th width="">Payment Mode</th>
                            <th width="10%">Ride booked time</th>
                            <th width="5%">Current Status</th>
                            <th width="8%">Ride Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $a = 1;
                        foreach($list as $ridenow){
                            ?>
                            <tr>
                                <td> <?php echo $a;?></td>
                                <td><a href="home.php?pages=trip-details&id=<?=$ridenow['ride_id']?>" ><span title="Full Details" class="bookind-id"> <?=$ridenow['ride_id']?> </span></a></td>
                                <td><?php
                                    $user_name = substr($ridenow['user_name'],0,2)."*****";
                                    $user_phone = substr($ridenow['user_phone'],0,2)."********";
                                    $user_email = substr($ridenow['user_email'],0,2)."********";
                                    echo nl2br($user_name."\n".$user_phone."\n".$user_email);
                                    ?></td>
                                <td>
                                    <?php  $driver_name = $ridenow['driver_name'];
                                    if($driver_name == "")
                                    { ?>
                                        <h4 style="color:red;">Not Assign</h4>

                                    <?php }else{
                                        $driver_name = substr($ridenow['driver_name'],0,2)."*****";
                                        $driver_phone = substr($ridenow['driver_phone'],0,2)."********";
                                        $driver_email = substr($ridenow['driver_email'],0,2)."********";
                                        echo nl2br($driver_name."\n".$driver_phone."\n".$driver_email);
                                    } ?></td>

                                <td>
                                    <?php
                                    $pickup_location = $ridenow['pickup_location'];
                                    echo $pickup_location;
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    $drop_location = $ridenow['drop_location'];
                                    echo $drop_location;
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    $payment_option_name = $ridenow['payment_option_name'];
                                    echo $payment_option_name;
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    $ride_date = $ridenow['ride_date'];
                                    echo $ride_date.",".$ridenow['ride_time'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    $ride_status = $ridenow['ride_status'];
                                    $timestap = $ridenow['last_time_stamp'];
                                    switch ($ride_status){
                                        case "1":
                                            echo nl2br("New Booking \n ".$timestap);
                                            break;
                                        case "2":
                                            echo nl2br("Cancelled By User  \n ".$timestap);
                                            break;
                                        case "3":
                                            echo nl2br("Accepted by Driver  \n ".$timestap);
                                            break;
                                        case "4":
                                            echo nl2br("Cancelled by driver  \n ".$timestap);
                                            break;
                                        case "5":
                                            echo nl2br("Driver Arrived  \n ".$timestap);
                                            break;
                                        case "6":
                                            echo nl2br("Trip Started  \n ".$timestap);
                                            break;
                                        case "7":
                                            echo nl2br("Trip Completed  \n ".$timestap);
                                            break;
                                        case "8":
                                            echo nl2br("Trip Book By Admin  \n ".$timestap);
                                            break;
                                        case "17":
                                            echo nl2br("Trip Cancel By Admin  \n ".$timestap);
                                            break;
                                        default:
                                            echo "----";
                                    }
                                    ?>
                                </td>

                                <td align="center">
                                    <?php
                                    $ride_status = $ridenow['ride_status'];
                                    if ($ride_status == 1)
                                    { ?>
                                        <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>

                                    <?php }else{ ?>
                                        <span data-target="#cancel<?php echo $ridenow['ride_id'];?>" data-toggle="modal"><a data-original-title="Cancel Ride"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_delete"> <i class="fa fa-times"></i> </a></span>
                                        <a target="_blank" href="home.php?pages=track-ride&id=<?=$ridenow['ride_id']?>" data-original-title="Track" data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="ion-android-locate"></i> </a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                            $a++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var gmarkers1 = [];
    var markers1 = [];
    var infowindow = new google.maps.InfoWindow({
        content: ''
    });

    // Our markers
    markers1 = [
        <?php
        foreach($all_driver as $driver_map) { ?>
        ['<?php echo $driver_map['driver_id'] ?>','<?php echo $driver_map['driver_name']?>', <?php echo $driver_map['current_lat'] ?>,<?php echo $driver_map['current_long'] ?>,'<?php echo $driver_map['driver_status'] ?>','<?php echo $driver_map['map_icon'] ?>','<div style=" text-transform: capitalize; cursor:pointer;" class="map_details_click" onclick="showmapdetails(<?php echo $driver_map["driver_id"] ?>)" ><?php echo $driver_map['driver_name'] ?></div>'],
        <?php };?>
    ];


    function initialize() {
        var center = new google.maps.LatLng(<?php echo $admin_settings['admin_panel_latitude']?>, <?php echo $admin_settings['admin_panel_longitude']?>);
        var mapOptions = {
            zoom: 10,
            center: center,
            mapTypeId: google.maps.MapTypeId.TERRAIN
        };

        map = new google.maps.Map(document.getElementById('map'), mapOptions);
        directionsDisplay.setMap(map);
        var start = new google.maps.places.Autocomplete(
            (document.getElementById('origin-input')),
            {types: ['geocode']});
        var end = new google.maps.places.Autocomplete(
            (document.getElementById('destination-input')),
            {types: ['geocode']});

        end.addListener('place_changed', function () {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        });
        for (i = 0; i < markers1.length; i++) {
            addMarker(markers1[i]);
        }
    }
    function calculateAndDisplayRoute(directionsService, directionsDisplay) {

        directionsService.route({
            origin: document.getElementById('origin-input').value,
            destination: document.getElementById('destination-input').value,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
                //console.log(response.geometry.location.lat());
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }

    /**
     * Function to add marker to map
     */

    function addMarker(marker) {
        var category = marker[4];
        var title = marker[1];
        var pos = new google.maps.LatLng(marker[2], marker[3]);
        var content = marker[6];
        var icon = marker[5];
        marker1 = new google.maps.Marker({
            title: title,
            position: pos,
            category: category,
            icon: icon,
            map: map
        });

        gmarkers1.push(marker1);

        // Marker click listener
        google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
            return function () {
                infowindow.setContent(content);
                infowindow.open(map, marker1);
                map.panTo(this.getPosition());
                map.setZoom(15);
            }
        })(marker1, content));
    }

    /**
     * Function to filter markers by category
     */

    filterMarkers = function (category) {
        for (i = 0; i < markers1.length; i++) {
            marker = gmarkers1[i];
            // If is same category or category not picked
            if (marker.category == category || category.length === 0) {
                marker.setVisible(true);
            }
            // Categories don't match
            else {
                marker.setVisible(false);
            }
        }
    }

    // Init map
    initialize();

</script>
</section>
<!-- Main Content Ends -->

</body>
</html>