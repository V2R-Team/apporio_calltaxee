<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');
     $query="select * from car_type";
	$result = $db->query($query);
	$car=$result->rows;
	
	$query="select * from city";
    $result = $db->query($query);
    $city=$result->rows;

    $query="select * from rental_category";
	$result = $db->query($query);
	$list=$result->rows;
	if(isset($_POST['save'])) 
{
        $car_type_id = $_POST['car_type_id'];
        $city_id = $_POST['city_id'];
        $rental_category_id = $_POST['rental_category_id'];
        $price = $_POST['price'];
        $priceHRS = $_POST['price_phr'];
        $priceKMS = $_POST['price_pkm'];

        $query="select * from rentcard WHERE car_type_id='$car_type_id' AND city_id='$city_id' AND rental_category_id='$rental_category_id'";
	$result = $db->query($query);
        $ex_rows = $result->num_rows;
        if($ex_rows == 0)
        {
                $query2="INSERT INTO rentcard(car_type_id,city_id,rental_category_id,price,price_per_hrs,price_per_kms) VALUES ('$car_type_id','$city_id','$rental_category_id','$price','$priceHRS','$priceKMS')";
		$db->query($query2);
		$car_type_id = $db->getLastId();
                $msg = "Rent of Car Added Successfully";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
                $db->redirect("home.php?pages=add-rent");
        }else{
                $msg = "Rent of This Car Already Added";
                echo '<script type="text/javascript">alert("'.$msg.'")</script>';
                $db->redirect("home.php?pages=add-rent");
        }
}
?>    
<script>
   function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    
    function validatelogin() {
        var car_type_id = document.getElementById('car_type_id').value;
        var city_id = document.getElementById('city_id').value;
	var price = document.getElementById('price').value;
	var pricehrs = document.getElementById('pricehrs').value;
	var pricekms = document.getElementById('pricekms').value;
	var rental_category_id = document.getElementById('rental_category_id').value;
		if(city_id == "")
        {
            alert("Select City");
            return false;
        }
        if(car_type_id == "")
        {
            alert("Select Car Type");
            return false;
        }
		if(rental_category_id == "")
        {
            alert("Select Category");
            return false;
        }
         if(price == "")
        {
            alert("Enter Price");
            return false;
        }
        if(pricehrs == "")
        {
            alert("Enter Price Per Hour");
            return false;
        }
        if(pricekms == "")
        {
            alert("Enter Price Per Kilometers");
            return false;
        }
        
    }
</script>

  <div class="wraper container-fluid">
    <div class="page-title">
      <h3 class="title">Add Rent</h3>
        
      <span class="tp_rht">
           <a href="home.php?pages=rental-car" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Back"><i class="fa fa-reply"></i></a>
       </span>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          
          <div class="panel-body">
            <div class=" form" >
              <form class="cmxform form-horizontal tasi-form"  method="post" enctype="multipart/form-data" onSubmit="return validatelogin()">
			  
			       <div class="form-group ">
                          <label class="control-label col-lg-2">City*</label>
                             <div class="col-lg-6">
                                  <select class="form-control" name="city_id" id="city_id">
                                      <option value="">--Please Select City--</option>
                                      <?php foreach($city as $cityname): ?>
                                      <option id="<?php echo $cityname['city_id'];?>"  value="<?php echo $cityname['city_id'];?>"><?php echo $cityname['city_name']; ?></option>
                                 <?php endforeach; ?>
                                </select>
                        </div>
                    </div>
					
			    <div class="form-group ">
                  <label class="control-label col-lg-2">Car Type*</label>
                  <div class="col-lg-6">
                     <select class="form-control" name="car_type_id" id="car_type_id" >
                        <option value="">--Please Select Car Type--</option>
                          <?php foreach($car as $cartype){ ?>
                           <option id="<?php echo $cartype['car_type_id'];?>"  value="<?php echo $cartype['car_type_id'];?>"><?php echo $cartype['car_type_name']; ?></option>
                <?php } ?>
                    </select>
                  </div>
                </div>  
				
				<div class="form-group ">
                     <label class="control-label col-lg-2">Package Name*</label>
                        <div class="col-lg-6">
                           <select class="form-control" name="rental_category_id" id="rental_category_id">
                            <option value="">--Please Select Package--</option>
                             <?php foreach($list as $category): ?>
                               <option id="<?php echo $category['rental_category_id'];?>"  value="<?php echo $category['rental_category_id'];?>"><?php echo $category['rental_category']; ?></option>
                              <?php endforeach; ?>
                          </select>
                     </div>
                </div>
					
				
                <div class="form-group ">
                  <label for="lastname" class="control-label col-lg-2">Package Price*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control" placeholder="Package Price" name="price" onkeypress="return isNumber(event)" id="price"/>
                  </div>
                </div>

                  <div class="form-group ">
                  <label for="lastname" class="control-label col-lg-2">Price per Hour*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control" placeholder="Price per Hour After Travel Hours" name="price_phr" onkeypress="return isNumber(event)" id="pricehrs"/>
                  </div>
                </div>

                  <div class="form-group ">
                  <label for="lastname" class="control-label col-lg-2">Price per Kilometer*</label>
                  <div class="col-lg-6">
                    <input type="text" class="form-control" placeholder="Price per Kilometers After Package Travel Distance" name="price_pkm" onkeypress="return isNumber(event)" id="pricekms"/>
                  </div>
                </div>


				
                <div class="form-group">
                  <div class="col-lg-offset-2 col-lg-10">
                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                  </div>
                </div>
              </form>
            </div>
            <!-- .form --> 
            
          </div>
          <!-- panel-body --> 
        </div>
        <!-- panel --> 
      </div>
      <!-- col --> 
      
    </div>
    <!-- End row --> 
    
  </div>
  
  <!-- Page Content Ends --> 
  <!-- ================== --> 
  
</section>
<!-- Main Content Ends -->

</body>
</html>
