<?php
include_once '../apporioconfig/start_up.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

    $query="select * from user WHERE user_delete=0 ORDER BY user_id DESC";
    $result = $db->query($query);
    $list=$result->rows;
    if(!empty($list)){

        require_once 'PHPExcel.php';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'User ID');
        $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Riders Name');
        $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Email');
        $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Phone');
        $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Registration Date');
        $row = 2;
        foreach($list as $value)
        {
            $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['user_id']);
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['user_name']);
            $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['user_email']);
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['user_phone']);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['register_date']);
            $row++;
        }
        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header("Content-Disposition: attachment;filename=users.xlsx");
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');

    }else{
        echo '<script type="text/javascript">alert("No Data For Export")</script>';
        $db->redirect("home.php?pages=rider");
    }
?>