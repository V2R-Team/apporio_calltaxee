<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query='SELECT * FROM table_languages';
$result=$db->query($query);
$data=$result->rows;

?>


<form method="post"  name="frm">
    <div class="wraper container-fluid">

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><div class="row"><h2 class="col-md-4"> Edit Language</h2>
                            <div class="col-md-8" align="right">Select a language :
                                <form method="post">
                                    <select class="bootstrap-select" name="select_language" onchange="location=this.value;">
                                        <option disabled selected>Language Name</option>
                                        <?php foreach ($data as $language){ ?>
                                            <option value="home.php?pages=edit-languages&lang=<?php echo $language['language_id']?>"><?php echo $language['language_name']?></option>
                                        <?php } ?>
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
<h1>PLEASE SELECT A LANGUAGE</h1>
                        </div>
                    </div>
                </div>
        </div>
        <!-- End row -->

    </div>
</form>
</section>

<!-- Main Content Ends -->

</body></html>