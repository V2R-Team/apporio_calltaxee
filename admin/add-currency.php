<?php
include_once '../apporioconfig/start_up.php';
if(!isset($_SESSION['ADMIN']['ID']))
{
    $db->redirect("index.php");
}
include('common.php');

$query = "select * from All_Currencies";
$result = $db->query($query);
$all_curency = $result->rows;

$query = "select currency.id,currency.currency_html_code,currency.currency_unicode,currency.currency_isocode,All_Currencies.currency_name from currency INNER JOIN All_Currencies ON currency.currency_id=All_Currencies.id";
$result = $db->query($query);
$list = $result->rows;

if(isset($_POST['save']))
{
    $currency_id = $_POST['currency_id'];
    $query = "select * from currency WHERE currency_id='$currency_id'";
    $result = $db->query($query);
    $list1 = $result->row;
    if (empty($list1))
    {
        $currency_html_code = $_POST['currency_html_code'];
        $currency_unicode = $_POST['currency_unicode'];
        $currency_isocode = $_POST['currency_isocode'];
        $query2="INSERT INTO currency (currency_id,currency_html_code,currency_unicode,currency_isocode) VALUES ('$currency_id','$currency_html_code','$currency_unicode','$currency_isocode')";
        $db->query($query2);
        $msg = "Currency Added Successfully";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
        $db->redirect("home.php?pages=add-currency");
    }else{
        $msg = "Currency Already Added";
        echo '<script type="text/javascript">alert("'.$msg.'")</script>';
        $db->redirect("home.php?pages=add-currency");
    }
}
if(isset($_POST['savechanges']))
{
    $query2="UPDATE currency SET currency_html_code='".$_POST['currency_html_code']."',currency_unicode='".$_POST['currency_unicode']."',currency_isocode='".$_POST['currency_isocode']."' where id='".$_POST['savechanges']."'";
    $db->query($query2);
    $db->redirect("home.php?pages=add-currency");
}
?>

<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Add Currency</h3>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form">
                        <form class="cmxform form-horizontal tasi-form"  method="post"  onSubmit="return validatelogin()">
                            <div class="form-group ">
                                <label class="control-label col-lg-2">Select Currency*</label>
                                <div class="col-lg-6">
                                    <select class="form-control" name="currency_id" id="currency_id" required>
                                        <option value="">--Please Select Currency--</option>
                                        <?php foreach($all_curency as $curency): ?>
                                            <option value="<?php echo $curency['id'];?>"><?php echo $curency['currency_name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Currency Code Only In Html*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Currency Code Only In Html" name="currency_html_code" id="currency_html_code" required>
                                    <a target="_blank" href="https://www.toptal.com/designers/htmlarrows/currency/"><h5>For Html Code Plz Refer https://www.toptal.com/designers/htmlarrows/currency/ </h5></a>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Currency Unicode*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Currency Unicode" name="currency_unicode" id="currency_unicode" required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label class="control-label col-lg-2">Currency Iso Code*</label>
                                <div class="col-lg-6">
                                    <input type="text" class="form-control"  placeholder="Currency Iso Code" name="currency_isocode" id="currency_isocode" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <input type="submit" class=" btn btn-info col-md-4 col-sm-6 col-xs-12 black-background white" id="save" name="save" value="Save" >
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- .form -->

                </div>
                <!-- panel-body -->
            </div>
            <!-- panel -->
        </div>
        <!-- col -->

    </div>
    <!-- End row -->

</div>

<!-- Page Content Ends -->
<div class="wraper container-fluid">
    <div class="page-title">
        <h3 class="title">Currency</h3>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 mobtbl">
                            <table id="datatable" class="table table-striped table-bordered table-responsive">
                                <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Currency Name</th>
                                    <th>Currency Symbol</th>
                                    <th>Currency Unicode</th>
                                    <th>Currency Iso Code</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $i = 1;
                                foreach($list as $currency){?>
                                    <tr>
                                        <td><?= $i; ?></td>
                                        <td>
                                            <?php
                                            $name = $currency['currency_name'];
                                            echo $name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $code = $currency['currency_html_code'];
                                            echo $code;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $currency_unicode = $currency['currency_unicode'];
                                            echo $currency_unicode;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $currency_isocode = $currency['currency_isocode'];
                                            echo $currency_isocode;
                                            ?>
                                        </td>
                                        <td>
                                            <span data-target="#edit<?php echo $currency['id'];?>" data-toggle="modal"><a data-original-title="Edit"  data-toggle="tooltip" data-placement="top" class="btn menu-icon btn_edit"> <i class="fa fa-pencil"></i> </a></span>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- ================== -->
<?php foreach($list as $currency){ ?>
    <div class="modal fade" id="edit<?php echo $currency['id'];?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content starts-->
            <form method="post" >
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title fdetailsheading">Edit Currency</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Currency Html Code</label>
                                    <input type="text" class="form-control"  placeholder="Currency Html Code" name="currency_html_code" value="<?php echo $currency['currency_html_code'];?>" id="currency_html_code" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Currency Unicode</label>
                                    <input type="text" class="form-control"  placeholder="Currency Unicode" name="currency_unicode" value="<?php echo $currency['currency_unicode'];?>" id="currency_unicode" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="field-3" class="control-label">Currency Isocode</label>
                                    <input type="text" class="form-control"  placeholder="Currency Unicode" name="currency_isocode" value="<?php echo $currency['currency_isocode'];?>" id="currency_isocode" required>
                                </div>
                            </div>



                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" name="savechanges" value="<?php echo $currency['id'];?>" class="btn btn-info">Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php }?>
</section>
<!-- Main Content Ends -->

</body>
</html>
