<?php
include_once '../apporioconfig/start_up.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$query="select * from table_driver_bill INNER JOIN driver ON table_driver_bill.driver_id=driver.driver_id";
$result = $db->query($query);
$list = $result->rows;
$data = array();
foreach($list as $k => $v)
{
    $data[$v['driver_id']]['driver_id'] = $v['driver_id'];
    $data[$v['driver_id']]['driver_name'] = $v['driver_name'];
    $data[$v['driver_id']]['outstanding_amount'][] = $v['outstanding_amount'];
    $data[$v['driver_id']]['company_payment'] = $v['company_payment'];
}
if(!empty($data)){
    require_once 'PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Driver Name');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Billed Outstanding');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Unbilled Amount');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Total Outstanding');
    $row = 2;
    foreach($data as $value)
    {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['driver_name']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, array_sum($value['outstanding_amount']));
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['company_payment']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, array_sum($value['outstanding_amount'])+$value['company_payment']);
        $row++;
    }
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=account.xlsx");
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');

}else{
    echo '<script type="text/javascript">alert("No Data For Export")</script>';
    $db->redirect("home.php?pages=accounts");
}
?>