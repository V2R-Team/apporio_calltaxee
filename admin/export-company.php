<?php
include_once '../apporioconfig/start_up.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$query = "select * from company ORDER BY company_id DESC";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $key => $value) {
    $company_id = $value['company_id'];
    $query = "select * from driver WHERE company_id='$company_id'";
    $result = $db->query($query);
    $ex_rows = $result->num_rows;
    $list[$key] = $value;
    $list[$key]["total_driver"] = $ex_rows;
}
if(!empty($list)){

    require_once 'PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Company ID');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Company Name');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Company Email');
    $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Company Phone');
    $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Company Address');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Company Contact Person');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'No. of Drivers');
    $row = 2;
    foreach($list as $value)
    {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['company_id']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['company_name']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['company_email']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['company_phone']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['company_address']);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['company_contact_person']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['total_driver']);
        $row++;
    }
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=company.xlsx");
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');

}else{
    echo '<script type="text/javascript">alert("No Data For Export")</script>';
    $db->redirect("home.php?pages=view-company");
}
?>