<?php
/**
 * Created by PhpStorm.
 * Developer: Aamir Brar
 * Date: 2017-05-10
 * Time: 4:40 PM
 */
session_start();
include('common.php');
include_once '../apporioconfig/start_up.php';
session_destroy();
$db->redirect("home.php?pages=index");
?>