<?php
include_once '../apporioconfig/start_up.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$status = $_GET['status'];
switch ($status)
{
    case "1":
        $query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id WHERE ride_table.ride_status IN (1,3,5,6) ORDER BY ride_table.ride_id DESC";
        $result = $db->query($query);
        $list=$result->rows;
        foreach ($list as $key=>$value)
        {
            $driver_id = $value['driver_id'];
            $payment_option_id = $value['payment_option_id'];
            $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
            $result1 = $db->query($query1);
            $list12 = $result1->row;
            $payment_option_name = $list12['payment_option_name'];

            if($driver_id == 0)
            {
                $driver_name = "";
            }else{
                $query1 = "select * from driver where driver_id ='$driver_id'";
                $result1 = $db->query($query1);
                $list1 = $result1->row;
                $driver_name = $list1['driver_name'];
            }
            $list[$key]=$value;
            $list[$key]["driver_name"] = $driver_name;
            $list[$key]["payment_option_name"]=$payment_option_name;
        }
        if (!empty($list))
        {
            require_once 'PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Ride id');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Driver Name');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Ride Name');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Car Type');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Pickup Address');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Drop Address');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Payment Mode');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Ride booked time');
            $row = 2;
            foreach($list as $value)
            {
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['ride_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['driver_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['user_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['car_type_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['pickup_location']);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['drop_location']);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['payment_option_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['ride_date']);
                $row++;
            }
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=activerides.xlsx");
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        }else{
            echo '<script type="text/javascript">alert("No Data For Export")</script>';
            $db->redirect("home.php?pages=ride-now");
        }
        break;
    case "2":
        $query = "select * from ride_table INNER JOIN user ON ride_table.user_id=user.user_id INNER JOIN car_type ON ride_table.car_type_id=car_type.car_type_id INNER JOIN driver ON ride_table.driver_id=driver.driver_id WHERE ride_table.ride_status = 7 ORDER BY ride_table.ride_id DESC";
        $result = $db->query($query);
        $list = $result->rows;
        foreach ($list as $key => $value) {
            $payment_option_id = $value['payment_option_id'];
            $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
            $result1 = $db->query($query1);
            $list12 = $result1->row;
            $payment_option_name = $list12['payment_option_name'];
            $list[$key] = $value;
            $list[$key]["payment_option_name"] = $payment_option_name;
        }
        if (!empty($list))
        {
            require_once 'PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Ride id');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Driver Name');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Ride Name');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Car Type');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Pickup Address');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Drop Address');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Payment Mode');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Ride booked time');
            $row = 2;
            foreach($list as $value)
            {
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['ride_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['driver_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['user_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['car_type_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['pickup_location']);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['drop_location']);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['payment_option_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['ride_date']);
                $row++;
            }
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=completerides.xlsx");
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        }else{
            echo '<script type="text/javascript">alert("No Data For Export")</script>';
            $db->redirect("home.php?pages=ride-later");
        }
        break;
    case "3":
        $query = "select * from rental_booking INNER JOIN user ON rental_booking.user_id=user.user_id INNER JOIN car_type ON rental_booking.car_type_id=car_type.car_type_id where rental_booking.booking_type=1 AND booking_status IN (10,11,12,13) ORDER BY rental_booking.rental_booking_id";
        $result = $db->query($query);
        $list=$result->rows;
        foreach ($list as $key=>$value)
        {
            $driver_id = $value['driver_id'];
            $payment_option_id = $value['payment_option_id'];
            $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
            $result1 = $db->query($query1);
            $list12 = $result1->row;
            $payment_option_name = $list12['payment_option_name'];
            if($driver_id == 0)
            {
                $driver_name = "";
            }else{
                $query1 = "select * from driver where driver_id ='$driver_id'";
                $result1 = $db->query($query1);
                $list1 = $result1->row;
                $driver_name = $list1['driver_name'];
            }
            $list[$key]=$value;
            $list[$key]["driver_name"] = $driver_name;
            $list[$key]["payment_option_name"] = $payment_option_name;
        }
        if (!empty($list))
        {
            require_once 'PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Ride id');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Driver Name');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Ride Name');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Car Type');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Pickup Address');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Payment Mode');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Ride booked time');
            $row = 2;
            foreach($list as $value)
            {
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['rental_booking_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['driver_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['user_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['car_type_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['pickup_location']);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['payment_option_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['booking_date']);
                $row++;
            }
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=rentalactiverides.xlsx");
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        }else{
            echo '<script type="text/javascript">alert("No Data For Export")</script>';
            $db->redirect("home.php?pages=active-rental-rides");
        }
        break;
    case "4":
        $query = "select * from rental_booking INNER JOIN user ON rental_booking.user_id=user.user_id INNER JOIN car_type ON rental_booking.car_type_id=car_type.car_type_id where rental_booking.booking_type=1 AND booking_status IN (14,15,17,18,16) ORDER BY rental_booking.rental_booking_id";
        $result = $db->query($query);
        $list=$result->rows;
        foreach ($list as $key=>$value)
        {
            $driver_id = $value['driver_id'];
            $rental_booking_id = $value['rental_booking_id'];
            $payment_option_id = $value['payment_option_id'];
            $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
            $result1 = $db->query($query1);
            $list12 = $result1->row;
            $payment_option_name = $list12['payment_option_name'];
            if($driver_id == 0)
            {
                $driver_name = "";
            }else{
                $query1 = "select * from driver where driver_id ='$driver_id'";
                $result1 = $db->query($query1);
                $list1 = $result1->row;
                $driver_name = $list1['driver_name'];
            }
            $query1 = "select * from table_done_rental_booking where rental_booking_id ='$rental_booking_id'";
            $result1 = $db->query($query1);
            $list123 = $result1->row;
            $end_location = $list123['end_location'];

            $list[$key]=$value;
            $list[$key]["driver_name"] = $driver_name;
            $list[$key]["end_location"] =  $end_location;
            $list[$key]["payment_option_name"] = $payment_option_name;
        }
        if (!empty($list))
        {
            require_once 'PHPExcel.php';
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Ride id');
            $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Driver Name');
            $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Ride Name');
            $objPHPExcel->getActiveSheet()->setCellValue('D1', 'Car Type');
            $objPHPExcel->getActiveSheet()->setCellValue('E1', 'Pickup Address');
            $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Drop Location');
            $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Payment Mode');
            $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Ride booked time');
            $row = 2;
            foreach($list as $value)
            {
                $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['rental_booking_id']);
                $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['driver_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['user_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['car_type_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['pickup_location']);
                $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['end_location']);
                $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['payment_option_name']);
                $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['booking_date']);
                $row++;
            }
            $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=rentarides.xlsx");
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
        }else{
            echo '<script type="text/javascript">alert("No Data For Export")</script>';
            $db->redirect("home.php?pages=rental-ride");
        }
        break;
}