<?php
function AndroidPushNotificationCustomer($did, $msg,$ride_id,$ride_status)
{
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $app_id="1";

    $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

    $headers = array (
        'Authorization: key=AAAA1q0RLTU:APA91bGhqlGBFc5TIpY8Ixtmh5u7oPJGuMYt2f2x1Q6gxWP_01Q5Zh_DvWVyKR-GjBa_lA_Fwn40L4Vqk6Ks038VlsmMX9VYFB8qT7BdBgNKL0dEhCuq8JZ65TgH7UWowZFlZRzRTPkv',
        'Content-Type: application/json' );

    // Open connection
    $ch = curl_init ();
    // Set the url, number of POST vars, POST data
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS,  json_encode ( $fields ) );
    // Execute post
    $result = curl_exec ( $ch );
    // Close connection
    curl_close ( $ch );
    return $result;
}

function AndroidPushNotificationDriver($did, $msg,$ride_id,$ride_status) {
    // Set POST variables
    $url = 'https://fcm.googleapis.com/fcm/send';

    $app_id="2";



    $fields = array ('to' => $did,'priority' => 'high','data' => array('message' => $msg,'ride_id'=>$ride_id,'ride_status'=> $ride_status,'app_id'=>$app_id) );

    $headers = array (
        'Authorization: key=AAAA1q0RLTU:APA91bGhqlGBFc5TIpY8Ixtmh5u7oPJGuMYt2f2x1Q6gxWP_01Q5Zh_DvWVyKR-GjBa_lA_Fwn40L4Vqk6Ks038VlsmMX9VYFB8qT7BdBgNKL0dEhCuq8JZ65TgH7UWowZFlZRzRTPkv',
        'Content-Type: application/json' );

    // Open connection
    $ch = curl_init ();
    // Set the url, number of POST vars, POST data
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, true );
    curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
    // Execute post
    $result = curl_exec ( $ch );
    // Close connection
    curl_close ( $ch );

    return $result;

}
?>