<?php
include_once '../apporioconfig/start_up.php';
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$query ="select * from rental_payment ORDER BY rental_payment_id DESC";
$result = $db->query($query);
$list = $result->rows;
foreach ($list as $key=>$value)
{
    $rental_booking_id = $value['rental_booking_id'];
    $query="select * from rental_booking WHERE rental_booking_id='$rental_booking_id'";
    $result = $db->query($query);
    $list2=$result->row;
    $user_id = $list2['user_id'];
    $query="select * from table_done_rental_booking WHERE rental_booking_id='$rental_booking_id'";
    $result = $db->query($query);
    $list1=$result->row;
    $rental_package_price = $list1['final_bill_amount'];
    $coupan_price = $list1['coupan_price'];
    $driver_id = $list1['driver_id'];
    $payment_option_id = $list2['payment_option_id'];
    $query1 = "select * from payment_option where payment_option_id ='$payment_option_id'";
    $result1 = $db->query($query1);
    $list12 = $result1->row;
    $payment_option_name = $list12['payment_option_name'];
    $query="select * from driver WHERE driver_id='$driver_id'";
    $result = $db->query($query);
    $list4=$result->row;
    $driver_name = $list4['driver_name'];
    $city_id = $list4['city_id'];
    $query="select * from city WHERE city_id='$city_id'";
    $result = $db->query($query);
    $list5=$result->row;
    $currency = $list5['currency'];

    $user_booking_date_time = $list2['user_booking_date_time'];
    $query="select * from user WHERE user_id='$user_id'";
    $result = $db->query($query);
    $list3=$result->row;
    $user_name = $list3['user_name'];

    $list[$key] = $value;
    $list[$key]["user_booking_date_time"] = $user_booking_date_time;
    $list[$key]["user_name"] = $user_name;
    $list[$key]["driver_name"] = $driver_name;
    $list[$key]["currency"] = $currency;
    $list[$key]["rental_package_price"] = $rental_package_price;
    $list[$key]["coupan_price"] = $coupan_price;
    $list[$key]["payment_method"] = $payment_option_name;
}
if(!empty($list)){
    require_once 'PHPExcel.php';
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getActiveSheet()->setCellValue('A1', 'User Name');
    $objPHPExcel->getActiveSheet()->setCellValue('B1', 'Driver Name');
    $objPHPExcel->getActiveSheet()->setCellValue('C1', 'Total Amount');
    $objPHPExcel->getActiveSheet()->setCellValue('F1', 'Package Price');
    $objPHPExcel->getActiveSheet()->setCellValue('G1', 'Coupan Price');
    $objPHPExcel->getActiveSheet()->setCellValue('H1', 'Payment Date');
    $objPHPExcel->getActiveSheet()->setCellValue('I1', 'Payment Mode');

    $row = 2;
    foreach($list as $value)
    {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['user_name']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['driver_name']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['amount_paid']);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['rental_package_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['coupan_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['user_booking_date_time']);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['payment_method']);
        $row++;
    }
    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header("Content-Disposition: attachment;filename=rentaltransactions.xlsx");
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');

}else{
    echo '<script type="text/javascript">alert("No Data For Export")</script>';
    $db->redirect("home.php?pages=rental-transactions");
}