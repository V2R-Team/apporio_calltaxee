-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 27, 2021 at 06:08 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apporio_calltaxee`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(255) NOT NULL,
  `admin_lname` varchar(255) NOT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_cover` varchar(255) NOT NULL,
  `admin_password` varchar(255) NOT NULL,
  `admin_phone` varchar(255) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_desc` varchar(255) NOT NULL,
  `admin_add` varchar(255) NOT NULL,
  `admin_country` varchar(255) NOT NULL,
  `admin_state` varchar(255) NOT NULL,
  `admin_city` varchar(255) NOT NULL,
  `admin_zip` int(8) NOT NULL,
  `admin_skype` varchar(255) NOT NULL,
  `admin_fb` varchar(255) NOT NULL,
  `admin_tw` varchar(255) NOT NULL,
  `admin_goo` varchar(255) NOT NULL,
  `admin_insta` varchar(255) NOT NULL,
  `admin_dribble` varchar(255) NOT NULL,
  `admin_role` varchar(255) NOT NULL,
  `admin_type` int(11) NOT NULL DEFAULT '0',
  `admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_img`, `admin_cover`, `admin_password`, `admin_phone`, `admin_email`, `admin_desc`, `admin_add`, `admin_country`, `admin_state`, `admin_city`, `admin_zip`, `admin_skype`, `admin_fb`, `admin_tw`, `admin_goo`, `admin_insta`, `admin_dribble`, `admin_role`, `admin_type`, `admin_status`) VALUES
(1, 'Apporio', 'Infolabs', 'infolabs', 'uploads/admin/user.png', '', 'apporio7788', '9560506619', 'hello@apporio.com', 'Apporio Infolabs Pvt. Ltd. is an ISO certified mobile application and web application development company in India. We provide end to end solution from designing to development of the software. ', '#467, Spaze iTech Park', 'India', 'Haryana', 'Gurugram', 122018, 'Keshav', 'https://www.facebook.com/apporio/', '', '', '', '', '1', 1, 1),
(36, 'Fareed', 'Wardak', 'Fareed', '', '', 'AdminFareed', '925.523.8100', 'Fareed.Wardak@SmartPayWeb.com', '', '', '', '', '', 0, '', '', '', '', '', '', '1', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_panel_settings`
--

CREATE TABLE `admin_panel_settings` (
  `admin_panel_setting_id` int(11) NOT NULL,
  `admin_panel_name` varchar(255) NOT NULL,
  `admin_panel_logo` varchar(255) NOT NULL,
  `admin_panel_email` varchar(255) NOT NULL,
  `admin_panel_city` varchar(255) NOT NULL,
  `admin_panel_map_key` varchar(255) NOT NULL,
  `admin_panel_latitude` varchar(255) NOT NULL,
  `admin_panel_longitude` varchar(255) NOT NULL,
  `admin_panel_firebase_id` varchar(255) NOT NULL,
  `admin_panel_request` int(11) NOT NULL,
  `admin_panel_rideus_request` int(11) NOT NULL,
  `admin_panel_stripe_key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin_panel_settings`
--

INSERT INTO `admin_panel_settings` (`admin_panel_setting_id`, `admin_panel_name`, `admin_panel_logo`, `admin_panel_email`, `admin_panel_city`, `admin_panel_map_key`, `admin_panel_latitude`, `admin_panel_longitude`, `admin_panel_firebase_id`, `admin_panel_request`, `admin_panel_rideus_request`, `admin_panel_stripe_key`) VALUES
(1, 'CallTaxee', 'uploads/logo/logo_5a0012f7c7014.gif', 'Support@CallTaxee.com', 'Pleasanton, CA, United States', 'AIzaSyDtDu--fwhDZxVQKnCSZODbTAyZrnw5tn4', '37.6624312', '-121.8746789', 'calltaxee', 1, 10, 'sk_test_tp8pHbIHhvLlTaJ1180WtUbd');

-- --------------------------------------------------------

--
-- Table structure for table `All_Currencies`
--

CREATE TABLE `All_Currencies` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `All_Currencies`
--

INSERT INTO `All_Currencies` (`id`, `currency_name`) VALUES
(1, 'ARIARY'),
(2, 'AUSTRAL'),
(3, 'BHAT'),
(4, 'BIRR'),
(5, 'BOLIVAR'),
(6, 'BOLIVIANO'),
(7, 'CEDI'),
(8, 'CENT'),
(9, 'COLON'),
(10, 'CORDOBA'),
(11, 'CRUZEIRO'),
(12, 'DALASI'),
(13, 'DINAR'),
(14, 'DIRHAM'),
(15, 'DOBRA'),
(16, 'DOLLAR'),
(17, 'DONG'),
(18, 'DRACHMA'),
(19, 'DRAM'),
(20, 'ESCUDO'),
(21, 'EURO'),
(22, 'FLORIN'),
(23, 'FORINT'),
(24, 'FRANC'),
(25, 'GOURDE'),
(26, 'GUARANI'),
(27, 'GUILDER'),
(28, 'HRYVNIA'),
(29, 'KINA'),
(30, 'KIP'),
(31, 'KORUNA'),
(32, 'KRONE'),
(33, 'KUNA'),
(34, 'KWACHA'),
(35, 'KWANZA'),
(36, 'KYAT'),
(37, 'LARI'),
(38, 'LEK'),
(39, 'LEMPIRA'),
(40, 'LEONE'),
(41, 'LEU'),
(42, 'LEV'),
(43, 'LILANGENI'),
(44, 'LIRA'),
(45, 'LIVRE TOURNOIS'),
(46, 'LOTI'),
(47, 'MANTA'),
(48, 'METICAL'),
(49, 'MILL'),
(50, 'NAIRA'),
(51, 'NAKFA'),
(52, 'NGULTRM'),
(53, 'OUGUIYA'),
(54, 'PAANGA'),
(55, 'PATACA'),
(56, 'PENNY'),
(57, 'PESETA'),
(58, 'PESO'),
(59, 'POUND'),
(60, 'PULA'),
(61, 'QUETZAL'),
(62, 'RAND'),
(63, 'REAL'),
(64, 'RIAL'),
(65, 'RIEL'),
(66, 'RINGGIT'),
(67, 'RIYAL'),
(68, 'RUBEL'),
(69, 'RUFIYAA'),
(70, 'RUPEE'),
(71, 'RUPIAH'),
(72, 'SHEQEL'),
(73, 'SHILING'),
(74, 'SOL'),
(75, 'SOM'),
(76, 'SOMONI'),
(77, 'SPESMILO'),
(78, 'STERLING'),
(79, 'TAKA'),
(80, 'TALA'),
(81, 'TENGE'),
(82, 'TUGRIK'),
(83, 'VATU'),
(84, 'WON'),
(85, 'YEN'),
(86, 'YUAN'),
(87, 'ZLOTY');

-- --------------------------------------------------------

--
-- Table structure for table `application_currency`
--

CREATE TABLE `application_currency` (
  `application_currency_id` int(11) NOT NULL,
  `currency_name` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_currency`
--

INSERT INTO `application_currency` (`application_currency_id`, `currency_name`, `currency_iso_code`, `currency_unicode`) VALUES
(1, 'DOLLAR SIGN', 'AUD', '0024');

-- --------------------------------------------------------

--
-- Table structure for table `application_version`
--

CREATE TABLE `application_version` (
  `application_version_id` int(11) NOT NULL,
  `ios_current_version` varchar(255) NOT NULL,
  `ios_mandantory_update` int(11) NOT NULL,
  `android_current_version` varchar(255) NOT NULL,
  `android_mandantory_update` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `application_version`
--

INSERT INTO `application_version` (`application_version_id`, `ios_current_version`, `ios_mandantory_update`, `android_current_version`, `android_mandantory_update`) VALUES
(1, '233', 1, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `booking-otp`
--

CREATE TABLE `booking-otp` (
  `otp_id` int(11) NOT NULL,
  `user_phone` varchar(255) NOT NULL DEFAULT '',
  `code` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `booking_allocated`
--

CREATE TABLE `booking_allocated` (
  `booking_allocated_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cancel_reasons`
--

CREATE TABLE `cancel_reasons` (
  `reason_id` int(11) NOT NULL,
  `reason_name` varchar(255) NOT NULL,
  `reason_name_arabic` varchar(255) NOT NULL,
  `reason_name_french` int(11) NOT NULL,
  `reason_type` int(11) NOT NULL,
  `cancel_reasons_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cancel_reasons`
--

INSERT INTO `cancel_reasons` (`reason_id`, `reason_name`, `reason_name_arabic`, `reason_name_french`, `reason_type`, `cancel_reasons_status`) VALUES
(2, 'Driver refused to come', '', 0, 1, 1),
(3, 'Driver is late', '', 0, 1, 1),
(13, 'i got lift', '', 0, 1, 1),
(14, 'tesr', '', 0, 1, 1),
(7, 'Other ', '', 0, 1, 1),
(8, 'Customer No Show, No Response to Text or Call', '', 0, 2, 1),
(12, 'Reject By Admin', '', 0, 3, 1),
(16, 'Problem with my car', '', 0, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `card_id` int(11) NOT NULL,
  `customer_id` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `car_make`
--

CREATE TABLE `car_make` (
  `make_id` int(11) NOT NULL,
  `make_name` varchar(255) NOT NULL,
  `make_img` varchar(255) NOT NULL,
  `make_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_make`
--

INSERT INTO `car_make` (`make_id`, `make_name`, `make_img`, `make_status`) VALUES
(1, 'BMW', 'uploads/car/car_1.png', 2),
(2, 'Suzuki', 'uploads/car/car_2.png', 2),
(3, 'Ferrari', 'uploads/car/car_3.png', 2),
(4, 'Lamborghini', 'uploads/car/car_4.png', 2),
(5, 'Mercedes', 'uploads/car/car_5.png', 2),
(6, 'Tesla', 'uploads/car/car_6.png', 2),
(10, 'Renault', 'uploads/car/car_10.jpg', 2),
(11, 'taxi', 'uploads/car/car_11.png', 2),
(12, 'taxi', 'uploads/car/car_12.jpg', 2),
(13, 'yamaha', 'uploads/car/car_13.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `car_model`
--

CREATE TABLE `car_model` (
  `car_model_id` int(11) NOT NULL,
  `car_model_name` varchar(255) NOT NULL,
  `car_model_name_arabic` varchar(255) NOT NULL,
  `car_model_name_french` varchar(255) NOT NULL,
  `car_make` varchar(255) NOT NULL,
  `car_model` varchar(255) NOT NULL,
  `car_year` varchar(255) NOT NULL,
  `car_color` varchar(255) NOT NULL,
  `car_model_image` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_model`
--

INSERT INTO `car_model` (`car_model_id`, `car_model_name`, `car_model_name_arabic`, `car_model_name_french`, `car_make`, `car_model`, `car_year`, `car_color`, `car_model_image`, `car_type_id`, `car_model_status`) VALUES
(51, 'Tesla', '', '', 'Tesla', '', '', '', '', 34, 1),
(50, 'yamaha', '', '', 'yamaha', '', '', '', '', 33, 1),
(48, 'Mercedes', '', '', 'Mercedes', '', '', '', '', 30, 1),
(46, 'Ferrari', '', '', 'Ferrari', '', '', '', '', 29, 1),
(45, 'Suzuki', '', '', 'Suzuki', '', '', '', '', 28, 1),
(49, 'taxi', '', '', 'taxi', '', '', '', '', 31, 1);

-- --------------------------------------------------------

--
-- Table structure for table `car_type`
--

CREATE TABLE `car_type` (
  `car_type_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `car_type_name` varchar(255) NOT NULL,
  `car_description` text NOT NULL,
  `car_name_arabic` varchar(255) NOT NULL,
  `car_type_name_french` varchar(255) NOT NULL,
  `car_type_image` varchar(255) NOT NULL,
  `cartype_image_size` varchar(255) NOT NULL,
  `advance_booking` int(11) NOT NULL,
  `advance_booking_time` varchar(255) NOT NULL,
  `cartype_big_image` varchar(255) NOT NULL DEFAULT '',
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `car_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `car_type`
--

INSERT INTO `car_type` (`car_type_id`, `service_id`, `car_type_name`, `car_description`, `car_name_arabic`, `car_type_name_french`, `car_type_image`, `cartype_image_size`, `advance_booking`, `advance_booking_time`, `cartype_big_image`, `ride_mode`, `car_admin_status`) VALUES
(28, 2, 'ECab', 'Economy Compacts', '', '', 'uploads/car/car_28.png', '190446', 0, '', '', 1, 1),
(29, 2, 'SCab', 'Standrad Full Size', '', '', 'uploads/car/car_29.png', '190446', 0, '', '', 1, 1),
(30, 3, 'XCab', 'SUV and Minivan', '', '', 'uploads/car/car_30.png', '190446', 0, '', '', 1, 1),
(31, 3, 'LXCab', 'Luxury full Size', '', '', 'uploads/car/car_31.png', '190446', 0, '', '', 1, 1),
(33, 4, 'UXCab', 'Black SUV & Mini Limo', '', '', 'uploads/car/car_33.png', '190446', 1, '2:00 ', '', 1, 1),
(34, 5, 'Wheel Access', 'Whellchair Accessible', '', '', 'uploads/car/car_34.png', '190446', 0, '', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `city_latitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_longitude` varchar(255) CHARACTER SET latin1 NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `currency_iso_code` varchar(255) NOT NULL,
  `currency_unicode` varchar(255) NOT NULL,
  `distance` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_arabic` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_name_french` varchar(255) CHARACTER SET latin1 NOT NULL,
  `city_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `city_name`, `city_latitude`, `city_longitude`, `currency_id`, `currency`, `currency_iso_code`, `currency_unicode`, `distance`, `city_name_arabic`, `city_name_french`, `city_admin_status`) VALUES
(3, 'Dummy City', '', '', 16, '&#x24;', 'AUD', '0024', 'Miles', '', '', 1),
(56, 'Gurugram', '', '', 16, '&#x24;', 'AUD', '0024', 'Miles', '', '', 1),
(129, 'Delhi', '28.7040592', '77.1024902', 16, '&#x24;', 'USD ', '00024', 'Km', '', '', 1),
(130, 'Pleasanton', '37.6624312', '-121.8746789', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(131, 'Fremont', '37.5482697', '-121.9885719', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(132, 'Hayward', '37.6688205', '-122.0807964', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(133, 'Oakland', '37.8043637', '-122.2711137', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(134, 'San Francisco', '37.7749295', '-122.4194155', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(135, 'Livermore', '37.6818745', '-121.7680088', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(136, 'Dublin', '37.7021521', '-121.9357918', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(137, 'Walnut Creek', '37.9100783', '-122.0651819', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(138, 'Concord', '37.9779776', '-122.0310733', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1),
(139, 'San Jose', '37.3382082', '-121.8863286', 6, '&#x24;', 'USD ', '00024', 'Miles', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_email` varchar(255) NOT NULL,
  `company_phone` varchar(255) NOT NULL,
  `company_address` varchar(255) NOT NULL,
  `country_id` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `company_contact_person` varchar(255) NOT NULL,
  `company_password` varchar(255) NOT NULL,
  `vat_number` varchar(255) NOT NULL,
  `company_image` varchar(255) NOT NULL,
  `company_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(101) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `skypho` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, '+93'),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, '+355'),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, '+213'),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, '+1684'),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, '+376'),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, '+244'),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, '+1264'),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, '+0'),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, '+1268'),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, '+54'),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, '+374'),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, '+297'),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, '+61'),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, '+43'),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, '+994'),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, '+1242'),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, '+973'),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, '+880'),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, '+1246'),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, '+375'),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, '+32'),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, '+501'),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, '+229'),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, '+1441'),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, '+975'),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, '+591'),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, '+387'),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, '+267'),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, '+0'),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, '+55'),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, '+246'),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, '+673'),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, '+359'),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, '+226'),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, '+257'),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, '+855'),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, '+237'),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, '+1'),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, '+238'),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, '+1345'),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, '+236'),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, '+235'),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, '+56'),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, '+86'),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, '+61'),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, '+672'),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, '+57'),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, '+269'),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, '+242'),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, '+242'),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, '+682'),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, '+506'),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, '+225'),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, '+385'),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, '+53'),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, '+357'),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, '+420'),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, '+45'),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, '+253'),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, '+1767'),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, '+1809'),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, '+593'),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, '+20'),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, '+503'),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, '+240'),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, '+291'),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, '+372'),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, '+251'),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, '+500'),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, '+298'),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, '+679'),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, '+358'),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, '+33'),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, '+594'),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, '+689'),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, '+0'),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, '+241'),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, '+220'),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, '+995'),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, '+49'),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, '+233'),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, '+350'),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, '+30'),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, '+299'),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, '+1473'),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, '+590'),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, '+1671'),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, '+502'),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, '+224'),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, '+245'),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, '+592'),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, '+509'),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, '+0'),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, '+39'),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, '+504'),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, '+852'),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, '+36'),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, '+354'),
(99, 'IN', 'INDIA', 'India', 'IND', 356, '+91'),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, '+62'),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, '+98'),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, '+964'),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, '+353'),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, '+972'),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, '+39'),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, '+1876'),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, '+81'),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, '+962'),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, '+7'),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, '+254'),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, '+686'),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, '+850'),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, '+82'),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, '+965'),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, '+996'),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, '+856'),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, '+371'),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, '+961'),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, '+266'),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, '+231'),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, '+218'),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, '+423'),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, '+370'),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, '+352'),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, '+853'),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, '+389'),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, '+261'),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, '+265'),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, '+60'),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, '+960'),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, '+223'),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, '+356'),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, '+692'),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, '+596'),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, '+222'),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, '+230'),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, '+269'),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, '+52'),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, '+691'),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, '+373'),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, '+377'),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, '+976'),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, '+1664'),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, '+212'),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, '+258'),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, '+95'),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, '+264'),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, '+674'),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, '+977'),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, '+31'),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, '+599'),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, '+687'),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, '+64'),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, '+505'),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, '+227'),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, '+234'),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, '+683'),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, '+672'),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, '+1670'),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, '+47'),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, '+968'),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, '++92'),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, '+680'),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, '+970'),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, '+507'),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, '+675'),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, '+595'),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, '+51'),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, '+63'),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, '+0'),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, '+48'),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, '+351'),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, '+1787'),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, '+974'),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, '+262'),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, '+40'),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, '+70'),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, '+250'),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, '+290'),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, '+1869'),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, '+1758'),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, '+508'),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, '+1784'),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, '+684'),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, '+378'),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, '+239'),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, '+966'),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, '+221'),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, '+381'),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, '+248'),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, '+232'),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, '+65'),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, '+421'),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, '+386'),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, '+677'),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, '+252'),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, '+27'),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, '+0'),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, '+34'),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, '+94'),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, '+249'),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, '+597'),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, '+47'),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, '+268'),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, '+46'),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, '+41'),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, '+963'),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, '+886'),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, '+992'),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, '+255'),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, '+66'),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, '+670'),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, '+228'),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, '+690'),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, '+676'),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, '+1868'),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, '+216'),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, '+90'),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, '+7370'),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, '+1649'),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, '+688'),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, '+256'),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, '+380'),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, '+971'),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, '+44'),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, '+1'),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, '+1'),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, '+598'),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, '+998'),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, '+678'),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, '+58'),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, '+84'),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, '+1284'),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, '+1340'),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, '+681'),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, '+212'),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, '+967'),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, '+260'),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, '+263');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `coupons_id` int(11) NOT NULL,
  `coupons_code` varchar(255) NOT NULL,
  `coupons_price` varchar(255) NOT NULL,
  `total_usage_limit` int(11) NOT NULL,
  `per_user_limit` int(11) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `expiry_date` varchar(255) NOT NULL,
  `coupon_type` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon_type`
--

CREATE TABLE `coupon_type` (
  `coupon_type_id` int(11) NOT NULL,
  `coupon_type_name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coupon_type`
--

INSERT INTO `coupon_type` (`coupon_type_id`, `coupon_type_name`, `status`) VALUES
(1, 'Nominal', 1),
(2, 'Percentage', 1);

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `currency_id` int(100) DEFAULT NULL,
  `currency_html_code` varchar(100) DEFAULT NULL,
  `currency_unicode` varchar(100) DEFAULT NULL,
  `currency_isocode` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `currency_id`, `currency_html_code`, `currency_unicode`, `currency_isocode`) VALUES
(6, 16, '&#x24;', '00024', 'USD ');

-- --------------------------------------------------------

--
-- Table structure for table `customer_support`
--

CREATE TABLE `customer_support` (
  `customer_support_id` int(11) NOT NULL,
  `application` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `done_ride`
--

CREATE TABLE `done_ride` (
  `done_ride_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `arrived_time` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `waiting_time` varchar(255) NOT NULL DEFAULT '0',
  `waiting_price` varchar(255) NOT NULL DEFAULT '0',
  `ride_time_price` varchar(255) NOT NULL DEFAULT '0',
  `peak_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `night_time_charge` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0',
  `company_commision` varchar(255) NOT NULL DEFAULT '0',
  `driver_amount` varchar(255) NOT NULL DEFAULT '0',
  `amount` varchar(255) NOT NULL,
  `wallet_deducted_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `distance` varchar(255) NOT NULL,
  `meter_distance` varchar(255) NOT NULL,
  `tot_time` varchar(255) NOT NULL,
  `total_payable_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `payment_status` int(11) NOT NULL,
  `payment_falied_message` varchar(255) NOT NULL,
  `rating_to_customer` int(11) NOT NULL,
  `rating_to_driver` int(11) NOT NULL,
  `done_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `done_ride`
--

INSERT INTO `done_ride` (`done_ride_id`, `ride_id`, `begin_lat`, `begin_long`, `end_lat`, `end_long`, `begin_location`, `end_location`, `arrived_time`, `begin_time`, `end_time`, `waiting_time`, `waiting_price`, `ride_time_price`, `peak_time_charge`, `night_time_charge`, `coupan_price`, `driver_id`, `total_amount`, `company_commision`, `driver_amount`, `amount`, `wallet_deducted_amount`, `distance`, `meter_distance`, `tot_time`, `total_payable_amount`, `payment_status`, `payment_falied_message`, `rating_to_customer`, `rating_to_driver`, `done_date`) VALUES
(1, 1, '28.4123259', '77.0434147', '28.4123259', '77.0434147', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '02:06:52 PM', '02:06:53 PM', '02:06:57 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 2, '10', '0.40', '9.60', '10.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 0, 1, '0000-00-00'),
(2, 3, '28.4122832', '77.0434371', '28.4122853', '77.0434596', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:10:56 AM', '09:11:07 AM', '09:11:15 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 0, 1, '0000-00-00'),
(3, 4, '28.4122853', '77.0434596', '28.4122853', '77.0434596', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:11:03 PM', '12:11:05 PM', '12:11:08 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 0, 0, '0000-00-00'),
(4, 5, '28.4122976', '77.0434709', '28.4123078', '77.0434596', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '11:28:57 AM', '11:28:59 AM', '11:29:16 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 0, 0, '0000-00-00'),
(5, 6, '28.4123483', '77.0434147', '28.4123078', '77.0434596', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '11:49:53 AM', '11:50:02 AM', '11:50:22 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.04 Miles', '0.0', '0', '0.00', 1, '', 0, 0, '0000-00-00'),
(6, 7, '28.4122976', '77.0434709', '28.4122976', '77.0434709', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:01:41 PM', '12:01:43 PM', '12:01:57 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '0.00', 1, '', 0, 1, '0000-00-00'),
(7, 8, '28.4122976', '77.0434709', '28.4122976', '77.0434709', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '12:18:47 PM', '12:18:50 PM', '12:19:00 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 1, '0000-00-00'),
(8, 9, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '12:23:32 PM', '12:23:35 PM', '12:23:57 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 0, '0000-00-00'),
(9, 11, '', '', '', '', '', '', '07:37:57 AM', '', '', '0', '0', '0', '0.00', '0.00', '0.00', 4, '0', '0', '0', '', '0.00', '', '', '', '0.00', 0, '', 0, 1, '0000-00-00'),
(10, 12, '37.6942347', '-121.9329311', '37.7050199', '-121.9287302', '6035 Foothill Rd, Pleasanton, CA 94588, USA', '753 Dublin Blvd, Dublin, CA 94568, USA', '08:37:06 AM', '08:39:03 AM', '08:43:29 AM', '1', '2.00', '00.00', '0.00', '0.00', '0.00', 4, '7', '0.70', '6.30', '5.00', '0.00', '1.42 Miles', '100.0', '4', '7', 1, '', 0, 1, '0000-00-00'),
(11, 13, '37.7050143', '-121.9287475', '37.6938917', '-121.9322611', '753 Dublin Blvd, Dublin, CA 94568, USA', '7701-7999 Deodar Way, Pleasanton, CA 94588, USA', '08:52:14 AM', '08:52:19 AM', '08:56:53 AM', '0', '0.00', '1.00', '0.00', '0.00', '0.00', 4, '6', '0.60', '5.40', '5.00', '0.00', '1.45 Miles', '2009.9999999999998', '5', '6', 1, '', 0, 1, '0000-00-00'),
(12, 14, '37.6938717', '-121.9322795', '37.7118964', '-121.7216695', '7701-7999 Deodar Way, Pleasanton, CA 94588, USA', '6264 Northfront Rd, Livermore, CA 94551, USA', '09:51:36 AM', '10:05:39 AM', '10:18:30 AM', '14', '28.00', '9.00', '0.00', '0.00', '0.00', 4, '81.25', '8.13', '73.12', '44.25', '0.00', '12.85 Miles', '18950.0', '13', '81.25', 1, '', 0, 1, '0000-00-00'),
(13, 15, '28.4123483', '77.0434147', '28.4123483', '77.0434147', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '04:58:17 AM', '04:58:19 AM', '04:58:31 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 1, '0000-00-00'),
(14, 17, '28.4123078', '77.0434596', '28.4123078', '77.0434596', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '11:22:46 AM', '11:22:49 AM', '11:22:52 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 1, '0000-00-00'),
(15, 26, '28.4123406', '77.043447', '28.412286', '77.0434263', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:37:26 AM', '09:37:29 AM', '09:37:36 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 0, '0000-00-00'),
(16, 27, '28.4123249', '77.0434534', '28.4123236', '77.0434435', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '09:45:22 AM', '09:45:25 AM', '09:45:42 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 0, '0000-00-00'),
(17, 28, '28.4123387', '77.0434507', '28.4123388', '77.0434513', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '02:01:11 PM', '02:01:14 PM', '02:01:16 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 0, '0000-00-00'),
(18, 29, '28.4123079', '77.0434434', '28.4123377', '77.0434544', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '02:15:05 PM', '02:15:10 PM', '02:15:13 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 0, '0000-00-00'),
(19, 30, '28.4123451', '77.0434591', '28.4123451', '77.0434591', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '02:16:54 PM', '02:16:58 PM', '02:17:00 PM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 0, '0000-00-00'),
(20, 31, '28.4123478', '77.0434561', '28.4123481', '77.0434553', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '06:21:58 AM', '06:22:00 AM', '06:22:03 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 0, '0000-00-00'),
(21, 32, '28.4123495', '77.0434591', '28.4123494', '77.0434716', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '06:48:05 AM', '06:48:06 AM', '06:48:10 AM', '0', '0.00', '00.00', '0.00', '0.00', '0.00', 3, '5', '0.50', '4.50', '5.00', '0.00', '0.00 Miles', '0.0', '0', '5', 1, '', 0, 0, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `driver`
--

CREATE TABLE `driver` (
  `driver_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `driver_email` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `driver_image` varchar(255) NOT NULL,
  `driver_password` varchar(255) NOT NULL,
  `driver_token` text NOT NULL,
  `total_payment_eraned` varchar(255) NOT NULL DEFAULT '0',
  `company_payment` varchar(255) NOT NULL DEFAULT '0',
  `driver_payment` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `car_model_id` int(11) NOT NULL,
  `car_number` varchar(255) NOT NULL,
  `city_id` int(11) NOT NULL,
  `register_date` varchar(255) NOT NULL,
  `license` text NOT NULL,
  `license_expire` varchar(10) NOT NULL,
  `rc` text NOT NULL,
  `rc_expire` varchar(10) NOT NULL,
  `insurance` text NOT NULL,
  `insurance_expire` varchar(10) NOT NULL,
  `other_docs` text NOT NULL,
  `driver_bank_name` varchar(255) NOT NULL,
  `driver_account_number` varchar(255) NOT NULL,
  `total_card_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `total_cash_payment` varchar(255) NOT NULL DEFAULT '0.00',
  `amount_transfer_pending` varchar(255) NOT NULL DEFAULT '0.00',
  `current_lat` varchar(255) NOT NULL,
  `current_long` varchar(255) NOT NULL,
  `current_location` varchar(255) NOT NULL,
  `last_update` varchar(255) NOT NULL,
  `last_update_date` varchar(255) NOT NULL,
  `completed_rides` int(255) NOT NULL DEFAULT '0',
  `reject_rides` int(255) NOT NULL DEFAULT '0',
  `cancelled_rides` int(255) NOT NULL DEFAULT '0',
  `login_logout` int(11) NOT NULL,
  `busy` int(11) NOT NULL,
  `online_offline` int(11) NOT NULL,
  `detail_status` int(11) NOT NULL,
  `payment_transfer` int(11) NOT NULL DEFAULT '0',
  `verification_date` varchar(255) NOT NULL,
  `verification_status` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `driver_signup_date` date NOT NULL,
  `driver_status_image` varchar(255) NOT NULL DEFAULT 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png',
  `driver_status_message` varchar(1000) NOT NULL DEFAULT 'your document id in under process, You will be notified very soon',
  `total_document_need` int(11) NOT NULL,
  `driver_admin_status` int(11) NOT NULL DEFAULT '1',
  `verfiy_document` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver`
--

INSERT INTO `driver` (`driver_id`, `company_id`, `commission`, `driver_name`, `driver_email`, `driver_phone`, `driver_image`, `driver_password`, `driver_token`, `total_payment_eraned`, `company_payment`, `driver_payment`, `device_id`, `flag`, `rating`, `car_type_id`, `car_model_id`, `car_number`, `city_id`, `register_date`, `license`, `license_expire`, `rc`, `rc_expire`, `insurance`, `insurance_expire`, `other_docs`, `driver_bank_name`, `driver_account_number`, `total_card_payment`, `total_cash_payment`, `amount_transfer_pending`, `current_lat`, `current_long`, `current_location`, `last_update`, `last_update_date`, `completed_rides`, `reject_rides`, `cancelled_rides`, `login_logout`, `busy`, `online_offline`, `detail_status`, `payment_transfer`, `verification_date`, `verification_status`, `unique_number`, `driver_signup_date`, `driver_status_image`, `driver_status_message`, `total_document_need`, `driver_admin_status`, `verfiy_document`) VALUES
(2, 0, 4, 'sahil', 'sahil@gmail.com', '9797546446', '', '123456', 'yjRJUIt1hEpgwpUK', '9.6', '0.4', '', 'dsc_mAxNHp8:APA91bE8qyyCKx_Ol7u4b6elPOFgdWw_qFxufBhPaOoLG6DDb09mAjmwO5dgNJ6sbyeAkLvpCrFNxOMKUbftTFZ4YoCAFK41cbCCkxCRvc5DJAm307dNjV18oq-Ijmz2EEzQC2o_j41A', 2, '4.5', 2, 3, '123456', 56, 'Saturday, Oct 28', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.4122785', '77.0434253', '----', '08:06', 'Monday, Oct 30, 2017', 2, 0, 0, 2, 0, 1, 2, 0, '', 1, '', '2017-10-28', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(3, 0, 10, 'manish', 'manish@gmail.com', '977976646464', '', '123456', 'FByY9KhhJ3YVXLYF', '73.57', '8.18', '', 'dsc_mAxNHp8:APA91bE8qyyCKx_Ol7u4b6elPOFgdWw_qFxufBhPaOoLG6DDb09mAjmwO5dgNJ6sbyeAkLvpCrFNxOMKUbftTFZ4YoCAFK41cbCCkxCRvc5DJAm307dNjV18oq-Ijmz2EEzQC2o_j41A', 2, '3.96153846154', 31, 49, '123456', 56, 'Monday, Oct 30', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '28.412271', '77.0434164', '----', '06:50', 'Tuesday, Nov 21, 2017', 16, 0, 0, 1, 0, 1, 2, 0, '', 1, '', '2017-10-30', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(4, 0, 10, 'mark', 'fred.wardak@gmail.com', '15104002000', '', 'mark1234', 'xWtoEbG3AVz2ganA', '87.47', '9.73', '', 'ekKpjXM1mYw:APA91bHInFaiXzkJ_y2YkTqtZeBWmmab9iD9W8v4CbA2_fKwOa8TB5HfrhO-_Q8LIafvLACfFdbyzEXvXPnGtg5YNjYf8LHzP7Hu3_7lhJe_C1jhZguSb8iuEskotkbRJCvYi7hJhaZ-', 2, '5', 31, 49, '289', 130, 'Thursday, Nov 2', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '37.6940875', '-121.9327382', '----', '07:44', 'Thursday, Nov 30, 2017', 3, 1, 8, 1, 0, 1, 2, 0, '', 1, '', '2017-11-02', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0),
(5, 0, 15, 'fareed', 'fred.wardak@smarttaxiweb.com', '9255238100', '', '9255238100', '4Rv87OQrxfIc2cec', '0', '0', '', 'c1hKV_s8FME:APA91bH1yYKW1m1M2L9Qm_WLYNdAzEBi10s_7niWgr3VDzn5a27chQk70hbMR_Cr9i5yLMMRcFaobY6coV2xAsPQRfz1g5qGmjaHqrQRspDGzGqR_boAV82vOX6plOrmlSiHOo24pWcX', 2, '', 31, 49, '289', 130, 'Wednesday, Nov 29', '', '', '', '', '', '', '', '', '', '0.00', '0.00', '0.00', '37.7145098', '-121.725453', '----', '08:26', 'Wednesday, Nov 29, 2017', 0, 0, 0, 2, 0, 0, 2, 0, '', 1, '', '2017-11-29', 'http://www.apporiotaxi.com/superadminpanel/images/icons8-Error-80.png', 'your document id in under process, You will be notified very soon', 3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `driver_earnings`
--

CREATE TABLE `driver_earnings` (
  `driver_earning_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `total_amount` varchar(255) NOT NULL,
  `rides` int(11) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `ride_mode` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `driver_earnings`
--

INSERT INTO `driver_earnings` (`driver_earning_id`, `driver_id`, `total_amount`, `rides`, `amount`, `outstanding_amount`, `ride_mode`, `date`) VALUES
(1, 2, '10', 1, '9.60', '0.40', 1, '2017-10-28'),
(2, 3, '10', 1, '9', '1', 1, '2017-10-30'),
(3, 3, '26', 1, '23.4', '2.6', 1, '2017-10-31'),
(4, 4, '97.2', 1, '87.47', '9.73', 1, '2017-11-02'),
(5, 3, '10.75', 1, '9.67', '1.08', 1, '2017-11-03'),
(6, 3, '25', 1, '22.5', '2.5', 1, '2017-11-17'),
(7, 3, '10', 1, '9', '1', 1, '2017-11-18');

-- --------------------------------------------------------

--
-- Table structure for table `driver_ride_allocated`
--

CREATE TABLE `driver_ride_allocated` (
  `driver_ride_allocated_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_ride_allocated`
--

INSERT INTO `driver_ride_allocated` (`driver_ride_allocated_id`, `driver_id`, `ride_id`, `ride_mode`) VALUES
(1, 2, 1, 1),
(2, 3, 32, 1),
(3, 4, 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `extra_charges`
--

CREATE TABLE `extra_charges` (
  `extra_charges_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `extra_charges_type` int(11) NOT NULL,
  `extra_charges_day` varchar(255) NOT NULL,
  `slot_one_starttime` varchar(255) NOT NULL,
  `slot_one_endtime` varchar(255) NOT NULL,
  `slot_two_starttime` varchar(255) NOT NULL,
  `slot_two_endtime` varchar(255) NOT NULL,
  `payment_type` int(11) NOT NULL,
  `slot_price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `fav_id` int(11) NOT NULL,
  `driver_id` int(65) NOT NULL,
  `user_id` int(65) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `favourites`
--

INSERT INTO `favourites` (`fav_id`, `driver_id`, `user_id`) VALUES
(3, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `favourites_destinations`
--

CREATE TABLE `favourites_destinations` (
  `favourites_destination_id` int(11) NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `favourites_destinations`
--

INSERT INTO `favourites_destinations` (`favourites_destination_id`, `latitude`, `longitude`, `location`, `user_id`) VALUES
(10, '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 5),
(9, '30.7333148', '76.7794179', 'Chandigarh', 1),
(12, '37.6575115', '-121.88059729999999', 'Pleasanton Library', 6);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` char(49) CHARACTER SET utf8 DEFAULT NULL,
  `iso_639-1` char(2) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `iso_639-1`) VALUES
(1, 'English', 'en'),
(2, 'Afar', 'aa'),
(3, 'Abkhazian', 'ab'),
(4, 'Afrikaans', 'af'),
(5, 'Amharic', 'am'),
(6, 'Arabic', 'ar'),
(7, 'Assamese', 'as'),
(8, 'Aymara', 'ay'),
(9, 'Azerbaijani', 'az'),
(10, 'Bashkir', 'ba'),
(11, 'Belarusian', 'be'),
(12, 'Bulgarian', 'bg'),
(13, 'Bihari', 'bh'),
(14, 'Bislama', 'bi'),
(15, 'Bengali/Bangla', 'bn'),
(16, 'Tibetan', 'bo'),
(17, 'Breton', 'br'),
(18, 'Catalan', 'ca'),
(19, 'Corsican', 'co'),
(20, 'Czech', 'cs'),
(21, 'Welsh', 'cy'),
(22, 'Danish', 'da'),
(23, 'German', 'de'),
(24, 'Bhutani', 'dz'),
(25, 'Greek', 'el'),
(26, 'Esperanto', 'eo'),
(27, 'Spanish', 'es'),
(28, 'Estonian', 'et'),
(29, 'Basque', 'eu'),
(30, 'Persian', 'fa'),
(31, 'Finnish', 'fi'),
(32, 'Fiji', 'fj'),
(33, 'Faeroese', 'fo'),
(34, 'French', 'fr'),
(35, 'Frisian', 'fy'),
(36, 'Irish', 'ga'),
(37, 'Scots/Gaelic', 'gd'),
(38, 'Galician', 'gl'),
(39, 'Guarani', 'gn'),
(40, 'Gujarati', 'gu'),
(41, 'Hausa', 'ha'),
(42, 'Hindi', 'hi'),
(43, 'Croatian', 'hr'),
(44, 'Hungarian', 'hu'),
(45, 'Armenian', 'hy'),
(46, 'Interlingua', 'ia'),
(47, 'Interlingue', 'ie'),
(48, 'Inupiak', 'ik'),
(49, 'Indonesian', 'in'),
(50, 'Icelandic', 'is'),
(51, 'Italian', 'it'),
(52, 'Hebrew', 'iw'),
(53, 'Japanese', 'ja'),
(54, 'Yiddish', 'ji'),
(55, 'Javanese', 'jw'),
(56, 'Georgian', 'ka'),
(57, 'Kazakh', 'kk'),
(58, 'Greenlandic', 'kl'),
(59, 'Cambodian', 'km'),
(60, 'Kannada', 'kn'),
(61, 'Korean', 'ko'),
(62, 'Kashmiri', 'ks'),
(63, 'Kurdish', 'ku'),
(64, 'Kirghiz', 'ky'),
(65, 'Latin', 'la'),
(66, 'Lingala', 'ln'),
(67, 'Laothian', 'lo'),
(68, 'Lithuanian', 'lt'),
(69, 'Latvian/Lettish', 'lv'),
(70, 'Malagasy', 'mg'),
(71, 'Maori', 'mi'),
(72, 'Macedonian', 'mk'),
(73, 'Malayalam', 'ml'),
(74, 'Mongolian', 'mn'),
(75, 'Moldavian', 'mo'),
(76, 'Marathi', 'mr'),
(77, 'Malay', 'ms'),
(78, 'Maltese', 'mt'),
(79, 'Burmese', 'my'),
(80, 'Nauru', 'na'),
(81, 'Nepali', 'ne'),
(82, 'Dutch', 'nl'),
(83, 'Norwegian', 'no'),
(84, 'Occitan', 'oc'),
(85, '(Afan)/Oromoor/Oriya', 'om'),
(86, 'Punjabi', 'pa'),
(87, 'Polish', 'pl'),
(88, 'Pashto/Pushto', 'ps'),
(89, 'Portuguese', 'pt'),
(90, 'Quechua', 'qu'),
(91, 'Rhaeto-Romance', 'rm'),
(92, 'Kirundi', 'rn'),
(93, 'Romanian', 'ro'),
(94, 'Russian', 'ru'),
(95, 'Kinyarwanda', 'rw'),
(96, 'Sanskrit', 'sa'),
(97, 'Sindhi', 'sd'),
(98, 'Sangro', 'sg'),
(99, 'Serbo-Croatian', 'sh'),
(100, 'Singhalese', 'si'),
(101, 'Slovak', 'sk'),
(102, 'Slovenian', 'sl'),
(103, 'Samoan', 'sm'),
(104, 'Shona', 'sn'),
(105, 'Somali', 'so'),
(106, 'Albanian', 'sq'),
(107, 'Serbian', 'sr'),
(108, 'Siswati', 'ss'),
(109, 'Sesotho', 'st'),
(110, 'Sundanese', 'su'),
(111, 'Swedish', 'sv'),
(112, 'Swahili', 'sw'),
(113, 'Tamil', 'ta'),
(114, 'Telugu', 'te'),
(115, 'Tajik', 'tg'),
(116, 'Thai', 'th'),
(117, 'Tigrinya', 'ti'),
(118, 'Turkmen', 'tk'),
(119, 'Tagalog', 'tl'),
(120, 'Setswana', 'tn'),
(121, 'Tonga', 'to'),
(122, 'Turkish', 'tr'),
(123, 'Tsonga', 'ts'),
(124, 'Tatar', 'tt'),
(125, 'Twi', 'tw'),
(126, 'Ukrainian', 'uk'),
(127, 'Urdu', 'ur'),
(128, 'Uzbek', 'uz'),
(129, 'Vietnamese', 'vi'),
(130, 'Volapuk', 'vo'),
(131, 'Wolof', 'wo'),
(132, 'Xhosa', 'xh'),
(133, 'Yoruba', 'yo'),
(134, 'Chinese', 'zh'),
(135, 'Zulu', 'zu');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `message_name` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`m_id`, `message_id`, `message_name`, `language_id`) VALUES
(1, 1, 'Login Successful', 1),
(2, 1, 'Connexion rÃ©ussie', 2),
(3, 2, 'User inactive', 1),
(4, 2, 'Utilisateur inactif', 2),
(5, 3, 'Require fields Missing', 1),
(6, 3, 'Champs obligatoires manquants', 2),
(7, 4, 'Email-id or Password Incorrect', 1),
(8, 4, 'Email-id ou mot de passe incorrecte', 2),
(9, 5, 'Logout Successfully', 1),
(10, 5, 'DÃ©connexion rÃ©ussie', 2),
(11, 6, 'No Record Found', 1),
(12, 6, 'Aucun enregistrement TrouvÃ©', 2),
(13, 7, 'Signup Succesfully', 1),
(14, 7, 'Inscription effectuÃ©e avec succÃ¨s', 2),
(15, 8, 'Phone Number already exist', 1),
(16, 8, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(17, 9, 'Email already exist', 1),
(18, 9, 'Email dÃ©jÃ  existant', 2),
(19, 10, 'rc copy missing', 1),
(20, 10, 'Copie  carte grise manquante', 2),
(21, 11, 'License copy missing', 1),
(22, 11, 'Copie permis de conduire manquante', 2),
(23, 12, 'Insurance copy missing', 1),
(24, 12, 'Copie d\'assurance manquante', 2),
(25, 13, 'Password Changed', 1),
(26, 13, 'Mot de passe changÃ©', 2),
(27, 14, 'Old Password Does Not Matched', 1),
(28, 14, '\r\nL\'ancien mot de passe ne correspond pas', 2),
(29, 15, 'Invalid coupon code', 1),
(30, 15, '\r\nCode de Coupon Invalide', 2),
(31, 16, 'Coupon Apply Successfully', 1),
(32, 16, 'Coupon appliquÃ© avec succÃ¨s', 2),
(33, 17, 'User not exist', 1),
(34, 17, '\r\nL\'utilisateur n\'existe pas', 2),
(35, 18, 'Updated Successfully', 1),
(36, 18, 'Mis Ã  jour avec succÃ©s', 2),
(37, 19, 'Phone Number Already Exist', 1),
(38, 19, 'NumÃ©ro de tÃ©lÃ©phone dÃ©jÃ  existant', 2),
(39, 20, 'Online', 1),
(40, 20, 'En ligne', 2),
(41, 21, 'Offline', 1),
(42, 21, 'Hors ligne', 2),
(43, 22, 'Otp Sent to phone for Verification', 1),
(44, 22, ' Otp EnvoyÃ© au tÃ©lÃ©phone pour vÃ©rification', 2),
(45, 23, 'Rating Successfully', 1),
(46, 23, 'Ã‰valuation rÃ©ussie', 2),
(47, 24, 'Email Send Succeffully', 1),
(48, 24, 'Email EnvovoyÃ© avec succÃ©s', 2),
(49, 25, 'Booking Accepted', 1),
(50, 25, 'RÃ©servation acceptÃ©e', 2),
(51, 26, 'Driver has been arrived', 1),
(52, 26, 'Votre chauffeur est arrivÃ©', 2),
(53, 27, 'Ride Cancelled Successfully', 1),
(54, 27, 'RÃ©servation annulÃ©e avec succÃ¨s', 2),
(55, 28, 'Ride Has been Ended', 1),
(56, 28, 'Fin du Trajet', 2),
(57, 29, 'Ride Book Successfully', 1),
(58, 29, 'RÃ©servation acceptÃ©e avec succÃ¨s', 2),
(59, 30, 'Ride Rejected Successfully', 1),
(60, 30, 'RÃ©servation rejetÃ©e avec succÃ¨s', 2),
(61, 31, 'Ride Has been Started', 1),
(62, 31, 'DÃ©marrage du trajet', 2),
(63, 32, 'New Ride Allocated', 1),
(64, 32, 'Vous avez une nouvelle course', 2),
(65, 33, 'Ride Cancelled By Customer', 1),
(66, 33, 'RÃ©servation annulÃ©e par le client', 2),
(67, 34, 'Booking Accepted', 1),
(68, 34, 'RÃ©servation acceptÃ©e', 2),
(69, 35, 'Booking Rejected', 1),
(70, 35, 'RÃ©servation rejetÃ©e', 2),
(71, 36, 'Booking Cancel By Driver', 1),
(72, 36, 'RÃ©servation annulÃ©e par le chauffeur', 2);

-- --------------------------------------------------------

--
-- Table structure for table `no_driver_ride_table`
--

CREATE TABLE `no_driver_ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `ride_status` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL,
  `card_id` int(11) NOT NULL,
  `ride_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `no_driver_ride_table`
--

INSERT INTO `no_driver_ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `car_type_id`, `ride_type`, `ride_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_admin_status`) VALUES
(1, 1, '', '28.41229752351723', '77.04340696334839', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 30', '08:01:54', '08:01:54 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41229752351723,77.04340696334839&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1),
(2, 2, '', '37.69410325082573', '-121.93271152675152', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Tuesday, Oct 31', '07:25:39', '07:25:39 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.69410325082573,-121.93271152675152&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 28, 1, 1, 0, 1, 0, 1),
(3, 5, '', '37.6940791085708', '-121.93270716816187', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', '', '07:36:18', '07:36:18 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|37.6940791085708,-121.93270716816187&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1),
(4, 5, '', '37.69404589999999', '-121.93253169999998', '113, 5820 Stoneridge Mall Rd', '37.712559', '-121.721893', 'Livermore Toyota', '', '10:05:09', '10:05:09 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|37.69404589999999,-121.93253169999998&markers=color:red|label:D|37.712559,-121.721893&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1),
(5, 5, '', '37.69404589999999', '-121.93253169999998', '113, 5820 Stoneridge Mall Rd', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', '', '04:06:43', '04:06:43 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|37.69404589999999,-121.93253169999998&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1),
(6, 5, '', '37.69404589999999', '-121.93253169999998', '113, 5820 Stoneridge Mall Rd', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Friday, Nov 3', '04:06:51', '04:06:51 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.69404589999999,-121.93253169999998&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 30, 1, 1, 0, 1, 0, 1),
(7, 5, '', '37.71415240323637', '-121.72563292086126', '1130 Orchid St, Livermore, CA 94551, USA', '37.69404589999999', '-121.93253169999998', '113, 5820 Stoneridge Mall Rd', '', '18:24:20', '06:24:20 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|37.71415240323637,-121.72563292086126&markers=color:red|label:D|37.69404589999999,-121.93253169999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1),
(8, 5, '', '37.694106699718645', '-121.9327235966921', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', '', '08:24:20', '08:24:20 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|37.694106699718645,-121.9327235966921&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1),
(9, 6, '', '34.14385486613037', '-118.7940663099289', '30929 Minute Man Way, Westlake Village, CA 91361, USA', '37.6952657', '-121.92884490000002', 'Stoneridge Shopping Center', '', '07:02:45', '07:02:45 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|34.14385486613037,-118.7940663099289&markers=color:red|label:D|37.6952657,-121.92884490000002&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1),
(10, 6, '', '37.71460090193619', '-121.72539688646795', '5853 Lobelia Way, Livermore, CA 94551, USA', '37.6575115', '-121.88059729999999', 'Pleasanton Library', '', '08:06:36', '08:06:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|37.71460090193619,-121.72539688646795&markers=color:red|label:D|37.6575115,-121.88059729999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1),
(11, 6, '', '37.71460090193619', '-121.72539688646795', '5853 Lobelia Way, Livermore, CA 94551, USA', '37.6575115', '-121.88059729999999', 'Pleasanton Library', '', '08:08:07', '08:08:07 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=600x300&maptype=roadmap&markers=color:green|label:S|37.71460090193619,-121.72539688646795&markers=color:red|label:D|37.6575115,-121.88059729999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 31, 1, 1, 0, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `title_arabic` varchar(255) NOT NULL,
  `description_arabic` text NOT NULL,
  `title_french` varchar(255) NOT NULL,
  `description_french` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `name`, `title`, `description`, `title_arabic`, `description_arabic`, `title_french`, `description_french`) VALUES
(1, 'About Us', 'About Us', 'About Us and other information about company will go here.', '', '', '', ''),
(2, 'Help Center', 'Keshav Goyal', '+919560506619', '', '', '', ''),
(3, 'Terms and Conditions', 'Terms and Conditions', 'Company\'s terms and conditions will show here.', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `payment_confirm`
--

CREATE TABLE `payment_confirm` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `payment_platform` varchar(255) NOT NULL,
  `payment_amount` varchar(255) NOT NULL,
  `payment_date_time` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_confirm`
--

INSERT INTO `payment_confirm` (`id`, `order_id`, `user_id`, `payment_id`, `payment_method`, `payment_platform`, `payment_amount`, `payment_date_time`, `payment_status`) VALUES
(1, 1, 1, '1', 'Cash', 'Admin', '10', 'Saturday, Oct 28', 'Done'),
(2, 2, 1, '1', 'Cash', 'Admin', '5', 'Monday, Oct 30', 'Done'),
(3, 3, 1, '1', 'Cash', 'Admin', '5', 'Monday, Oct 30', 'Done'),
(4, 4, 1, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', 'Done'),
(5, 5, 1, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', 'Done'),
(6, 6, 1, '1', 'Cash', 'Admin', '5', 'Tuesday, Oct 31', 'Done'),
(7, 7, 1, '1', 'Cash', 'Android', '6.0', 'Anything', 'Anything'),
(8, 8, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything'),
(9, 10, 5, '1', 'Cash', 'Android', '8.75', 'Anything', 'Anything'),
(10, 11, 5, '1', 'Cash', 'Android', '7.2', 'Anything', 'Anything'),
(11, 12, 5, '1', 'Cash', 'Android', '81.25', 'Anything', 'Anything'),
(12, 13, 1, '1', 'Cash', 'Android', '5.75', 'Anything', 'Anything'),
(13, 14, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything'),
(14, 15, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything'),
(15, 16, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything'),
(16, 17, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything'),
(17, 18, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything'),
(18, 19, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything'),
(19, 20, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything'),
(20, 21, 1, '1', 'Cash', 'Android', '5', 'Anything', 'Anything');

-- --------------------------------------------------------

--
-- Table structure for table `payment_option`
--

CREATE TABLE `payment_option` (
  `payment_option_id` int(11) NOT NULL,
  `payment_option_name` varchar(255) NOT NULL,
  `payment_option_name_arabic` varchar(255) NOT NULL,
  `payment_option_name_french` varchar(255) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_option`
--

INSERT INTO `payment_option` (`payment_option_id`, `payment_option_name`, `payment_option_name_arabic`, `payment_option_name_french`, `status`) VALUES
(1, 'Cash', '', '', 1),
(2, 'Paypal', '', '', 1),
(3, 'Credit Card', '', '', 1),
(4, 'Wallet', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `price_card`
--

CREATE TABLE `price_card` (
  `price_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `distance_unit` varchar(255) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `now_booking_fee` int(11) NOT NULL,
  `later_booking_fee` int(11) NOT NULL,
  `cancel_fee` int(11) NOT NULL,
  `cancel_ride_now_free_min` int(11) NOT NULL,
  `cancel_ride_later_free_min` int(11) NOT NULL,
  `scheduled_cancel_fee` int(11) NOT NULL,
  `base_distance` varchar(255) NOT NULL,
  `base_distance_price` varchar(255) NOT NULL,
  `base_price_per_unit` varchar(255) NOT NULL,
  `free_waiting_time` varchar(255) NOT NULL,
  `wating_price_minute` varchar(255) NOT NULL,
  `free_ride_minutes` varchar(255) NOT NULL,
  `price_per_ride_minute` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `price_card`
--

INSERT INTO `price_card` (`price_id`, `city_id`, `distance_unit`, `currency`, `car_type_id`, `commission`, `now_booking_fee`, `later_booking_fee`, `cancel_fee`, `cancel_ride_now_free_min`, `cancel_ride_later_free_min`, `scheduled_cancel_fee`, `base_distance`, `base_distance_price`, `base_price_per_unit`, `free_waiting_time`, `wating_price_minute`, `free_ride_minutes`, `price_per_ride_minute`) VALUES
(53, 56, 'Miles', '&#x24;', 31, 10, 0, 0, 0, 0, 0, 0, '10', '5', '5', '2', '2', '5', '5'),
(52, 56, 'Miles', '&#x24;', 30, 10, 0, 0, 0, 0, 0, 0, '5', '5', '5', '2', '5', '5', '5'),
(51, 56, 'Miles', '&#x24;', 29, 10, 0, 0, 0, 0, 0, 0, '10', '5', '10', '5', '5', '5', '5'),
(50, 56, 'Miles', '&#x24;', 28, 10, 0, 0, 0, 0, 0, 0, '5', '5', '5', '5', '0', '5', '5'),
(54, 56, 'Miles', '&#x24;', 33, 10, 0, 0, 0, 0, 0, 0, '5', '5', '5', '5', '5', '5', '5'),
(55, 56, 'Miles', '&#x24;', 34, 10, 0, 0, 0, 0, 0, 0, '10', '5', '5', '5', '5', '5', '5'),
(56, 130, 'Miles', '&#x24;', 28, 10, 0, 0, 0, 0, 0, 0, '5', '100', '10', '0', '0', '0', '0'),
(57, 130, 'Miles', '&#x24;', 29, 10, 0, 0, 0, 0, 0, 0, '5', '5', '5', '5', '5', '5', '0'),
(58, 130, 'Miles', '&#x24;', 31, 15, 0, 0, 0, 0, 0, 0, '0', '4.95', '2.95', '1', '0.25', '0', '0.25'),
(59, 130, 'Miles', '&#x24;', 30, 10, 0, 0, 0, 0, 0, 0, '5', '5', '5', '5', '5', '5', '5'),
(60, 130, 'Miles', '&#x24;', 33, 5, 0, 0, 0, 0, 0, 0, '5', '5', '5', '5', '5', '0', '0'),
(61, 130, 'Miles', '&#x24;', 34, 1500, 0, 0, 0, 0, 0, 0, '10', '10', '5', '0', '0', '5', '5');

-- --------------------------------------------------------

--
-- Table structure for table `push_messages`
--

CREATE TABLE `push_messages` (
  `push_id` int(11) NOT NULL,
  `push_message_heading` text NOT NULL,
  `push_message` text NOT NULL,
  `push_image` varchar(255) NOT NULL,
  `push_web_url` text NOT NULL,
  `push_user_id` int(11) NOT NULL,
  `push_driver_id` int(11) NOT NULL,
  `push_messages_date` date NOT NULL,
  `push_app` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `push_messages`
--

INSERT INTO `push_messages` (`push_id`, `push_message_heading`, `push_message`, `push_image`, `push_web_url`, `push_user_id`, `push_driver_id`, `push_messages_date`, `push_app`) VALUES
(1, 'Fremont Rush', 'Many calls from Fremont Hub Area', 'uploads/notification/1500380956793.jpg', '', 0, 0, '2017-11-06', 2);

-- --------------------------------------------------------

--
-- Table structure for table `rental_booking`
--

CREATE TABLE `rental_booking` (
  `rental_booking_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rentcard_id` int(11) NOT NULL,
  `payment_option_id` int(11) DEFAULT '0',
  `coupan_code` varchar(255) DEFAULT '',
  `car_type_id` int(11) NOT NULL,
  `booking_type` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `start_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `start_meter_reading_image` varchar(255) NOT NULL,
  `end_meter_reading` varchar(255) NOT NULL DEFAULT '0',
  `end_meter_reading_image` varchar(255) NOT NULL,
  `booking_date` varchar(255) NOT NULL,
  `booking_time` varchar(255) NOT NULL,
  `user_booking_date_time` varchar(255) NOT NULL,
  `last_update_time` varchar(255) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `booking_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `booking_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rental_category`
--

CREATE TABLE `rental_category` (
  `rental_category_id` int(11) NOT NULL,
  `rental_category` varchar(255) NOT NULL,
  `rental_category_hours` varchar(255) NOT NULL,
  `rental_category_kilometer` varchar(255) NOT NULL,
  `rental_category_distance_unit` varchar(255) NOT NULL,
  `rental_category_description` longtext NOT NULL,
  `rental_category_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rental_category`
--

INSERT INTO `rental_category` (`rental_category_id`, `rental_category`, `rental_category_hours`, `rental_category_kilometer`, `rental_category_distance_unit`, `rental_category_description`, `rental_category_admin_status`) VALUES
(8, 'Apporio', '1', '10', 'Miles', '<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\"=\"\" !important;=\"\" font-size:=\"\" 13px;=\"\" font-style:=\"\" normal;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img class=\"alignnone size-full wp-image-434\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\" alt=\"iso_apporio\" width=\"232\" height=\"163\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n</h2>\r\n<a> </a>', 1),
(9, 'DUMMY', '4', '40', 'Km', '<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\"=\"\" !important;=\"\" font-size:=\"\" 13px;=\"\" font-style:=\"\" normal;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img class=\"alignnone size-full wp-image-434\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\" alt=\"iso_apporio\" width=\"232\" height=\"163\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n</h2>\r\n<a> </a>', 1),
(11, 'TOUR', '5', '100', 'Km', '<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Apporio<span style=\"color: inherit; font-family: inherit;\"> Infolabs Pvt. Ltd. is an ISO certified</span><br></h2>\r\n\r\n<h2 class=\"apporioh2\" style=\"font-family: \" open=\"\" sans\";=\"\" line-height:=\"\" 1.5;=\"\" margin:=\"\" 0px;=\"\" color:=\"\" rgb(0,=\"\" 0,=\"\" 0)=\"\" !important;=\"\" font-size:=\"\" 22px=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\"><br></h2>\r\n\r\n<h1><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Mobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.</p>\r\n\r\n<p class=\"product_details\" style=\"margin-top: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">Apporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.</p>\r\n\r\n<div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"></div>\r\n\r\n</div>\r\n\r\n</h1>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\">We have expertise on the following technologies :- Mobile Applications :<br><br><div class=\"row\" style=\"box-sizing: border-box; margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;=\"\" font-style:=\"\" normal;=\"\" font-variant-ligatures:=\"\" font-variant-caps:=\"\" font-weight:=\"\" letter-spacing:=\"\" orphans:=\"\" 2;=\"\" text-align:=\"\" start;=\"\" text-indent:=\"\" 0px;=\"\" text-transform:=\"\" none;=\"\" white-space:=\"\" widows:=\"\" word-spacing:=\"\" -webkit-text-stroke-width:=\"\" background-color:=\"\" rgb(255,=\"\" 255,=\"\" 255);=\"\" text-decoration-style:=\"\" initial;=\"\" text-decoration-color:=\"\" initial;\"=\"\"><div class=\"col-md-8\" style=\"box-sizing: border-box; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px; float: left; width: 780px;\"><p class=\"product_details\" style=\"box-sizing: border-box; margin: 0px; padding: 10px 0px; font-family: \" open=\"\" sans\"=\"\" !important;=\"\" font-size:=\"\" 13px;=\"\" font-style:=\"\" normal;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Native Android Application on Android Studio<br style=\"box-sizing: border-box;\">2. Native iOS Application on Objective-C<br style=\"box-sizing: border-box;\">3. Native iOS Application on Swift<br style=\"box-sizing: border-box;\">4. Titanium Appcelerator<br style=\"box-sizing: border-box;\">5. Iconic Framework</p>\r\n\r\n</div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.1; color: inherit; margin: 0px; font-size: 17px !important; padding: 10px 0px 5px !important; text-align: left;\">Web Application :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"box-sizing: border-box; font-family: inherit; font-weight: 500; line-height: 1.5; color: rgb(0, 0, 0) !important; margin: 0px; font-size: 22px !important; padding: 10px 0px 5px !important; text-align: left;\"><div class=\"row\" style=\"margin-right: -15px; margin-left: -15px; color: rgb(51, 51, 51); font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 14px;\"=\"\"><div class=\"col-md-8\" style=\"padding-right: 15px; padding-left: 15px; width: 780px;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-size: 13px; text-align: justify; line-height: 1.7; color: rgb(102, 102, 102) !important;\">1. Custom php web development<br>2. Php CodeIgnitor development<br>3. Cakephp web development<br>4. Magento development<br>5. Opencart development<br>6. WordPress development<br>7. Drupal development<br>8. Joomla Development<br>9. Woocommerce Development<br>10. Shopify Development</p>\r\n\r\n</div>\r\n\r\n<div class=\"col-md-4\" style=\"padding-right: 15px; padding-left: 15px; width: 390px;\"><img class=\"alignnone size-full wp-image-434\" src=\"http://apporio.com/wp-content/uploads/2016/10/iso_apporio.png\" alt=\"iso_apporio\" width=\"232\" height=\"163\"></div>\r\n\r\n</div>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">Marketing :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">1. Search Engine Optimization<br>2. Social Media Marketing and Optimization<br>3. Email and Content Marketing<br>4. Complete Digital Marketing</p>\r\n\r\n</h2>\r\n\r\n<h3 class=\"apporioh3\" style=\"font-family: \" open=\"\" sans\";=\"\" color:=\"\" rgb(51,=\"\" 51,=\"\" 51);=\"\" margin:=\"\" 0px;=\"\" font-size:=\"\" 17px=\"\" !important;=\"\" padding:=\"\" 10px=\"\" 0px=\"\" 5px=\"\" !important;\"=\"\">For more information, you can catch us on :</h3>\r\n\r\n<h2 class=\"apporioh2\" style=\"line-height: 1.5; margin: 0px; color: rgb(0, 0, 0) !important; font-size: 22px !important; padding: 10px 0px 5px !important;\"><p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\">mail : hello@apporio.com<br>phone : +91 95606506619<br>watsapp: +91 95606506619<br>Skype : keshavgoyal5</p>\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n<p class=\"product_details\" style=\"padding: 10px 0px; font-family: \" open=\"\" sans\";=\"\" font-size:=\"\" 13px;=\"\" text-align:=\"\" justify;=\"\" line-height:=\"\" 1.7;=\"\" color:=\"\" rgb(102,=\"\" 102,=\"\" 102)=\"\" !important;\"=\"\"><br></p>\r\n</h2>\r\n<a> </a>', 1),
(15, 'Platinum', '12', '200', 'Km', '<p>Applicable to trips within and around the following Locations: Lagos, Oyo, Ogun, Osun</p>', 1),
(17, '15 Seater ', '15', '200', 'Km', 'rgr rgh thty', 1),
(18, 'IBADAN TO LAGOS (RTN)', '15', '200', 'Km', 'WHOLE DAY CHARTER', 1),
(19, 'IBADAN TO LAGOS (DROP)', '4', '125', 'Km', 'DROP OFF', 1),
(20, 'Mrt', '1', '30', 'Km', 'test for', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rental_payment`
--

CREATE TABLE `rental_payment` (
  `rental_payment_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `amount_paid` varchar(255) NOT NULL,
  `payment_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rentcard`
--

CREATE TABLE `rentcard` (
  `rentcard_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `rental_category_id` int(11) NOT NULL,
  `price` varchar(255) NOT NULL,
  `price_per_hrs` int(11) NOT NULL,
  `price_per_kms` int(11) NOT NULL,
  `rentcard_admin_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rentcard`
--

INSERT INTO `rentcard` (`rentcard_id`, `city_id`, `car_type_id`, `rental_category_id`, `price`, `price_per_hrs`, `price_per_kms`, `rentcard_admin_status`) VALUES
(15, 56, 5, 10, '500', 10, 20, 1),
(18, 88, 4, 8, '6', 100, 2, 1),
(20, 3, 2, 12, '25000', 5000, 120, 1),
(21, 104, 20, 13, '50', 60, 80, 1),
(22, 78, 3, 15, '20000', 1000, 500, 1),
(24, 98, 4, 18, '45000', 0, 0, 1),
(25, 98, 4, 19, '30000', 0, 0, 1),
(26, 112, 3, 20, '130', 100, 5, 1),
(27, 121, 3, 8, '100', 10, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ride_allocated`
--

CREATE TABLE `ride_allocated` (
  `allocated_id` int(11) NOT NULL,
  `allocated_ride_id` int(11) NOT NULL,
  `allocated_driver_id` int(11) NOT NULL,
  `allocated_ride_status` int(11) NOT NULL DEFAULT '0',
  `allocated_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_allocated`
--

INSERT INTO `ride_allocated` (`allocated_id`, `allocated_ride_id`, `allocated_driver_id`, `allocated_ride_status`, `allocated_date`) VALUES
(1, 1, 2, 1, '2017-10-28'),
(2, 2, 3, 0, '2017-10-30'),
(3, 3, 3, 1, '2017-10-30'),
(4, 4, 3, 1, '2017-10-30'),
(5, 5, 3, 1, '2017-10-31'),
(6, 6, 3, 1, '2017-10-31'),
(7, 7, 3, 1, '2017-10-31'),
(8, 8, 3, 1, '2017-10-31'),
(9, 9, 3, 1, '2017-10-31'),
(10, 11, 4, 1, '2017-11-02'),
(11, 0, 4, 0, '2017-11-02'),
(12, 0, 4, 0, '2017-11-02'),
(13, 0, 4, 0, '2017-11-02'),
(14, 12, 4, 1, '2017-11-02'),
(15, 13, 4, 1, '2017-11-02'),
(16, 14, 4, 1, '2017-11-02'),
(17, 15, 3, 1, '2017-11-03'),
(18, 16, 3, 0, '2017-11-03'),
(19, 17, 3, 1, '2017-11-03'),
(20, 18, 4, 0, '2017-11-04'),
(21, 19, 4, 0, '2017-11-04'),
(22, 20, 4, 1, '2017-11-04'),
(23, 21, 4, 1, '2017-11-06'),
(24, 22, 4, 1, '0000-00-00'),
(25, 23, 4, 1, '2017-11-06'),
(26, 24, 4, 1, '2017-11-07'),
(27, 25, 4, 1, '2017-11-10'),
(28, 26, 3, 1, '2017-11-17'),
(29, 27, 3, 1, '2017-11-17'),
(30, 28, 3, 1, '2017-11-17'),
(31, 29, 3, 1, '2017-11-17'),
(32, 30, 3, 1, '2017-11-17'),
(33, 31, 3, 1, '2017-11-18'),
(34, 32, 3, 1, '2017-11-18');

-- --------------------------------------------------------

--
-- Table structure for table `ride_reject`
--

CREATE TABLE `ride_reject` (
  `reject_id` int(11) NOT NULL,
  `reject_ride_id` int(11) NOT NULL,
  `reject_driver_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_reject`
--

INSERT INTO `ride_reject` (`reject_id`, `reject_ride_id`, `reject_driver_id`) VALUES
(1, 18, 4);

-- --------------------------------------------------------

--
-- Table structure for table `ride_table`
--

CREATE TABLE `ride_table` (
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `pickup_lat` varchar(255) NOT NULL,
  `pickup_long` varchar(255) NOT NULL,
  `pickup_location` varchar(255) NOT NULL,
  `drop_lat` varchar(255) NOT NULL,
  `drop_long` varchar(255) NOT NULL,
  `drop_location` varchar(255) NOT NULL,
  `ride_date` varchar(255) NOT NULL,
  `ride_time` varchar(255) NOT NULL,
  `last_time_stamp` varchar(255) NOT NULL,
  `ride_image` text NOT NULL,
  `later_date` varchar(255) NOT NULL,
  `later_time` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `car_type_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL,
  `pem_file` int(11) NOT NULL DEFAULT '1',
  `ride_status` int(11) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `reason_id` int(11) NOT NULL,
  `payment_option_id` int(11) NOT NULL DEFAULT '1',
  `card_id` int(11) NOT NULL,
  `ride_platform` int(11) NOT NULL DEFAULT '1',
  `ride_admin_status` int(11) NOT NULL DEFAULT '1',
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ride_table`
--

INSERT INTO `ride_table` (`ride_id`, `user_id`, `coupon_code`, `pickup_lat`, `pickup_long`, `pickup_location`, `drop_lat`, `drop_long`, `drop_location`, `ride_date`, `ride_time`, `last_time_stamp`, `ride_image`, `later_date`, `later_time`, `driver_id`, `city_id`, `car_type_id`, `ride_type`, `pem_file`, `ride_status`, `payment_status`, `reason_id`, `payment_option_id`, `card_id`, `ride_platform`, `ride_admin_status`, `date`) VALUES
(1, 1, '', '28.41232583304304', '77.0434146746993', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Saturday, Oct 28', '14:06:44', '02:06:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41232583304304,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 2, 0, 2, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-28'),
(2, 1, '', '28.412273637348928', '77.04342640936375', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '', '', '', 'Monday, Oct 30', '08:12:31', '08:12:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412273637348928,77.04342640936375&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 0, 31, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-10-30'),
(3, 1, '', '28.412273637348928', '77.04342640936375', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.459080000000004', '77.07051799999999', 'Huda City Metro Station', 'Monday, Oct 30', '08:15:11', '09:11:15 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412273637348928,77.04342640936375&markers=color:red|label:D|,&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 2, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(4, 1, '', '28.412323768807028', '77.0433922111988', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Monday, Oct 30', '12:10:49', '12:11:08 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412323768807028,77.0433922111988&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-30'),
(5, 1, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '11:28:48', '11:29:16 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(6, 1, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '11:49:37', '11:50:22 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(7, 1, '', '28.412307844699374', '77.0434596017003', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '12:01:34', '12:01:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412307844699374,77.0434596017003&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(8, 1, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '12:18:36', '12:19:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(9, 1, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Tuesday, Oct 31', '12:23:22', '12:23:57 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-10-31'),
(10, 1, '', '28.4149034426345', '77.04391725361347', 'Block H, Sispal Vihar, Sector 49, Gurugram, Haryana 122001, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Wednesday, Nov 1', '05:27:27', '05:27:27 AM', '', '1/11/2017', '08:57', 0, 0, 33, 2, 1, 1, 0, 0, 1, 0, 1, 1, '2017-11-01'),
(11, 5, '', '37.6940791085708', '-121.93270716816187', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Thursday, Nov 2', '07:37:02', '08:00:37 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.6940791085708,-121.93270716816187&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-02'),
(12, 5, '', '37.694065578292616', '-121.93269845098256', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.704827', '-121.928623', 'Bank of the West', 'Thursday, Nov 2', '08:36:07', '08:43:29 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.694065578292616,-121.93269845098256&markers=color:red|label:D|37.704827,-121.928623&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(13, 5, '', '37.70505798270495', '-121.92871302366255', '753 Dublin Blvd, Dublin, CA 94568, USA', '37.69404589999999', '-121.93253169999998', '113, 5820 Stoneridge Mall Rd', 'Thursday, Nov 2', '08:51:39', '08:56:53 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.70505798270495,-121.92871302366255&markers=color:red|label:D|37.69404589999999,-121.93253169999998&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(14, 5, '', '37.69404589999999', '-121.93253169999998', '113, 5820 Stoneridge Mall Rd', '37.712559', '-121.721893', 'Livermore Toyota', 'Thursday, Nov 2', '09:51:14', '10:18:30 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.69404589999999,-121.93253169999998&markers=color:red|label:D|37.712559,-121.721893&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-02'),
(15, 1, '', '28.41359031747845', '77.04099699854851', '1106, Sohna Rd, Block S, Sispal Vihar, Sector 47, Gurugram, Haryana 122004, India', '28.4592693', '77.0724192', 'Huda Metro Station', 'Friday, Nov 3', '04:58:11', '04:58:31 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41359031747845,77.04099699854851&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-03'),
(16, 1, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.459080000000004', '77.07051799999999', 'Huda City Metro Station', 'Friday, Nov 3', '11:12:05', '11:12:05 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.459080000000004,77.07051799999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 0, 31, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-03'),
(17, 1, '', '28.412348244745605', '77.0434146746993', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.459080000000004', '77.07051799999999', 'Huda City Metro Station', 'Friday, Nov 3', '11:12:38', '11:22:52 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412348244745605,77.0434146746993&markers=color:red|label:D|28.459080000000004,77.07051799999999&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-03'),
(18, 5, '', '37.69410457732303', '-121.93272225558756', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Saturday, Nov 4', '08:30:41', '08:30:41 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.69410457732303,-121.93272225558756&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-04'),
(19, 5, '', '37.69410457732303', '-121.93272225558756', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Saturday, Nov 4', '08:34:32', '08:34:32 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.69410457732303,-121.93272225558756&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 0, 0, 31, 1, 1, 2, 0, 0, 1, 0, 1, 1, '2017-11-04'),
(20, 5, '', '37.69410457732303', '-121.93272225558756', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Saturday, Nov 4', '08:34:58', '08:38:26 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.69410457732303,-121.93272225558756&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-04'),
(21, 5, '', '37.69410643441919', '-121.9327235966921', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Monday, Nov 6', '06:11:27', '06:12:17 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.69410643441919,-121.9327235966921&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-06'),
(22, 7, '', '37.6708979', '-121.919401', 'Foothill High School, Pleasanton, CA, United States', '37.651234', '-122.101407', 'Southland Mall, Hayward, CA, United States', 'Monday, Nov 6', '06:31 AM', '06:33:26 AM', '', '', '', 4, 0, 31, 1, 1, 9, 0, 8, 1, 0, 2, 1, '2017-11-06'),
(23, 5, '', '37.69410590382028', '-121.93272192031147', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Monday, Nov 6', '09:01:18', '10:45:55 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.69410590382028,-121.93272192031147&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 2, 0, 3, 1, 0, 1, 1, '2017-11-06'),
(24, 5, '', '37.694088128754885', '-121.93272661417723', '113, 5820 Stoneridge Mall Rd, Pleasanton, CA 94588, USA', '37.6213129', '-122.37895540000001', 'San Francisco International Airport', 'Tuesday, Nov 7', '22:55:54', '10:56:54 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.694088128754885,-121.93272661417723&markers=color:red|label:D|37.6213129,-122.37895540000001&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 9, 0, 8, 1, 0, 1, 1, '2017-11-07'),
(25, 6, '', '37.704779800000004', '-121.9177336', 'Dublin Boulevard', '37.6952657', '-121.92884490000002', 'Stoneridge Shopping Center', 'Friday, Nov 10', '06:53:55', '06:55:19 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|37.704779800000004,-121.9177336&markers=color:red|label:D|37.6952657,-121.92884490000002&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 4, 0, 31, 1, 1, 2, 0, 7, 1, 0, 1, 1, '2017-11-10'),
(26, 1, '', '28.412247686937768', '77.04343546181917', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '09:37:09', '09:37:36 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412247686937768,77.04343546181917&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(27, 1, '', '28.412331435969133', '77.04344384372234', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '09:45:03', '09:45:42 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412331435969133,77.04344384372234&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(28, 1, '', '28.412337923567375', '77.04345289617777', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '14:00:56', '02:01:16 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412337923567375,77.04345289617777&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(29, 1, '', '28.41231728120792', '77.04345624893902', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '14:14:40', '02:15:13 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41231728120792,77.04345624893902&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(30, 1, '', '28.41231728120792', '77.04345624893902', '322B, Sohna Rd, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Friday, Nov 17', '14:16:47', '02:17:00 PM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41231728120792,77.04345624893902&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-17'),
(31, 1, '', '28.412349424308754', '77.0434619486332', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Saturday, Nov 18', '06:21:52', '06:22:03 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.412349424308754,77.0434619486332&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-18'),
(32, 1, '', '28.41235119365349', '77.04346932470798', 'IRIS Tech Park, Sector 49, Gurugram, Haryana 122018, India', '28.4592693', '77.0724192', 'Huda City Centre', 'Saturday, Nov 18', '06:47:57', '06:48:10 AM', 'https:maps.googleapis.com/maps/api/staticmap?center=&zoom=12&size=200x200&maptype=roadmap&markers=color:green|label:S|28.41235119365349,77.04346932470798&markers=color:red|label:D|28.4592693,77.0724192&key=AIzaSyAIFe17P91Mfez3T6cqk7hfDSyvMO812Z4', '', '', 3, 0, 31, 1, 1, 7, 1, 0, 1, 0, 1, 1, '2017-11-18');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`) VALUES
(2, 'E C O N O M Y'),
(3, 'P R E M I U M  L I N E'),
(4, 'U L T R A'),
(5, 'Wheelchair A C C E S S');

-- --------------------------------------------------------

--
-- Table structure for table `sos`
--

CREATE TABLE `sos` (
  `sos_id` int(11) NOT NULL,
  `sos_name` varchar(255) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `sos_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sos`
--

INSERT INTO `sos` (`sos_id`, `sos_name`, `sos_number`, `sos_status`) VALUES
(1, 'Police', '100', 1),
(3, 'keselamatan', '999', 1),
(4, 'ambulance', '101', 1),
(6, 'Breakdown', '199', 1),
(7, 'Breakdown', '199', 1),
(8, 'K10', '0800187241', 1),
(9, 'BOMBEROS', '911', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sos_request`
--

CREATE TABLE `sos_request` (
  `sos_request_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sos_number` varchar(255) NOT NULL,
  `request_date` varchar(255) NOT NULL,
  `application` int(11) NOT NULL,
  `latitude` varchar(255) NOT NULL,
  `longitude` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `suppourt`
--

CREATE TABLE `suppourt` (
  `sup_id` int(255) NOT NULL,
  `driver_id` int(255) NOT NULL,
  `name` text NOT NULL,
  `email` text NOT NULL,
  `phone` text NOT NULL,
  `query` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suppourt`
--

INSERT INTO `suppourt` (`sup_id`, `driver_id`, `name`, `email`, `phone`, `query`) VALUES
(1, 212, 'rohit', 'rohit', '8950200340', 'np'),
(2, 212, 'shilpa', 'shilpa', 'fgffchchch', 'yffhjkjhk'),
(3, 282, 'zak', 'zak', '0628926431', 'Hi the app driver is always crashing on android devices.also the gps  position is not taken by the application.regards'),
(4, 476, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SÓ CHAMAR NO WHATS ', 'SÓ CHAMAR NO WHATS '),
(5, 477, 'ANDRE ', 'andrefreitasalves2017@gmail.com', 'SEJA BEM VINDO', 'SEJA BEM VINDO'),
(6, 643, 'ewqla', 'lsajlas@jdsal', 'lsalk', 'lsalk');

-- --------------------------------------------------------

--
-- Table structure for table `table_documents`
--

CREATE TABLE `table_documents` (
  `document_id` int(11) NOT NULL,
  `document_name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_documents`
--

INSERT INTO `table_documents` (`document_id`, `document_name`) VALUES
(1, 'Driving License'),
(2, 'Vehicle Registration Certificate'),
(3, 'Polution'),
(4, 'Insurance '),
(5, 'Police Verification'),
(6, 'Permit of three vehiler '),
(7, 'HMV Permit'),
(8, 'Night NOC Drive'),
(10, 'Ø§Ù„Ø±Ø®ØµØ©'),
(11, 'Ø§Ù„Ø¨Ø·Ø§Ù‚Ø©'),
(12, 'ØµÙˆØ±Ø©'),
(13, 'cedula azul');

-- --------------------------------------------------------

--
-- Table structure for table `table_document_list`
--

CREATE TABLE `table_document_list` (
  `city_document_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `city_document_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_document_list`
--

INSERT INTO `table_document_list` (`city_document_id`, `city_id`, `document_id`, `city_document_status`) VALUES
(20, 3, 2, 1),
(19, 3, 1, 1),
(3, 56, 3, 1),
(4, 56, 2, 1),
(5, 56, 4, 1),
(23, 84, 8, 1),
(22, 84, 6, 1),
(21, 3, 4, 1),
(24, 130, 1, 1),
(25, 130, 2, 1),
(26, 130, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_done_rental_booking`
--

CREATE TABLE `table_done_rental_booking` (
  `done_rental_booking_id` int(11) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_arive_time` varchar(255) NOT NULL,
  `begin_lat` varchar(255) NOT NULL,
  `begin_long` varchar(255) NOT NULL,
  `begin_location` varchar(255) NOT NULL,
  `begin_date` varchar(255) NOT NULL,
  `begin_time` varchar(255) NOT NULL,
  `end_lat` varchar(255) NOT NULL,
  `end_long` varchar(255) NOT NULL,
  `end_location` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `end_time` varchar(255) NOT NULL,
  `total_distance_travel` varchar(255) NOT NULL DEFAULT '0',
  `total_time_travel` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_price` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_hours` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_hours_travel_charge` varchar(255) NOT NULL DEFAULT '0',
  `rental_package_distance` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel` varchar(50) NOT NULL DEFAULT '0',
  `extra_distance_travel_charge` varchar(255) NOT NULL,
  `total_amount` varchar(255) NOT NULL DEFAULT '0.00',
  `coupan_price` varchar(255) NOT NULL DEFAULT '0.00',
  `final_bill_amount` varchar(255) NOT NULL DEFAULT '0',
  `payment_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_bill`
--

CREATE TABLE `table_driver_bill` (
  `bill_id` int(11) NOT NULL,
  `bill_from_date` varchar(255) NOT NULL,
  `bill_to_date` varchar(255) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `outstanding_amount` varchar(255) NOT NULL,
  `bill_settle_date` date NOT NULL,
  `bill_status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_document`
--

CREATE TABLE `table_driver_document` (
  `driver_document_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `document_path` varchar(255) NOT NULL,
  `document_expiry_date` varchar(255) NOT NULL,
  `documnet_varification_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_driver_document`
--

INSERT INTO `table_driver_document` (`driver_document_id`, `driver_id`, `document_id`, `document_path`, `document_expiry_date`, `documnet_varification_status`) VALUES
(4, 2, 3, 'uploads/driver/1509165446document_image_23.jpg', '28-10-2017', 1),
(5, 2, 2, 'uploads/driver/1509165470document_image_22.jpg', '28-10-2017', 1),
(6, 2, 4, 'uploads/driver/1509165481document_image_24.jpg', '28-10-2017', 1),
(7, 3, 3, 'uploads/driver/1509350874document_image_33.jpg', '30-10-2017', 1),
(8, 3, 2, 'uploads/driver/1509350890document_image_32.jpg', '30-10-2017', 1),
(9, 3, 4, 'uploads/driver/1509350967document_image_34.jpg', '30-10-2017', 1),
(10, 4, 1, 'uploads/driver/1509607917document_image_41.jpg', '2-11-2017', 1),
(11, 4, 2, 'uploads/driver/1509607991document_image_42.jpg', '10-11-2017', 1),
(12, 4, 3, 'uploads/driver/1509608026document_image_43.jpg', '10-11-2017', 1),
(13, 5, 1, 'uploads/driver/1511982700document_image_51.jpg', '29-11-2017', 1),
(14, 5, 2, 'uploads/driver/1511982755document_image_52.jpg', '29-11-2017', 1),
(15, 5, 3, 'uploads/driver/1511982783document_image_53.jpg', '29-11-2017', 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_driver_online`
--

CREATE TABLE `table_driver_online` (
  `driver_online_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `online_time` varchar(255) NOT NULL,
  `offline_time` varchar(255) NOT NULL,
  `total_time` varchar(255) NOT NULL,
  `online_hour` int(11) NOT NULL,
  `online_min` int(11) NOT NULL,
  `online_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_driver_online`
--

INSERT INTO `table_driver_online` (`driver_online_id`, `driver_id`, `online_time`, `offline_time`, `total_time`, `online_hour`, `online_min`, `online_date`) VALUES
(1, 2, '2017-10-28 05:38:12', '', '', 0, 0, '2017-10-27'),
(2, 3, '2017-10-30 08:10:23', '2017-11-03 04:33:08', '20 Hours 22 Minutes', 20, 22, '2017-10-30'),
(3, 4, '2017-11-03 03:41:44', '2017-11-03 03:41:51', '17 Hours 141 Minutes', 17, 141, '2017-11-02'),
(4, 3, '2017-11-03 04:33:32', '', '', 0, 0, '2017-11-02'),
(5, 4, '2017-11-04 02:57:02', '2017-11-04 08:23:56', '5 Hours 59 Minutes', 5, 59, '2017-11-03'),
(6, 4, '2017-11-04 08:29:14', '2017-11-04 10:08:06', '1 Hours 38 Minutes', 1, 38, '2017-11-04'),
(7, 4, '2017-11-06 05:45:22', '2017-11-06 06:01:03', '0 Hours 15 Minutes', 0, 15, '2017-11-05'),
(8, 4, '2017-11-06 09:01:07', '2017-11-08 09:37:01', '2 Hours 92 Minutes', 2, 92, '2017-11-06'),
(9, 4, '2017-11-10 06:53:39', '2017-11-10 10:51:28', '3 Hours 57 Minutes', 3, 57, '2017-11-10'),
(10, 4, '2017-11-14 06:29:12', '2017-11-15 11:33:07', '5 Hours 3 Minutes', 5, 3, '2017-11-14'),
(11, 4, '2017-11-23 08:08:29', '', '', 0, 0, '2017-11-23');

-- --------------------------------------------------------

--
-- Table structure for table `table_languages`
--

CREATE TABLE `table_languages` (
  `language_id` int(11) NOT NULL,
  `language_name` varchar(255) NOT NULL,
  `language_status` int(2) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_languages`
--

INSERT INTO `table_languages` (`language_id`, `language_name`, `language_status`) VALUES
(42, 'Portuguese', 1),
(41, 'Vietnamese', 2),
(40, 'French', 2),
(39, 'Spanish', 1),
(38, 'Arabic', 1),
(37, 'Arabic', 1),
(36, 'Aymara', 1),
(35, 'Portuguese', 1),
(34, 'Russian', 2);

-- --------------------------------------------------------

--
-- Table structure for table `table_messages`
--

CREATE TABLE `table_messages` (
  `m_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `language_code` varchar(255) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_messages`
--

INSERT INTO `table_messages` (`m_id`, `message_id`, `language_code`, `message`) VALUES
(1, 1, 'en', 'Login SUCCESSFUL'),
(3, 2, 'en', 'User Inactive'),
(5, 3, 'en', 'Require fields Missing'),
(7, 4, 'en', 'Email-ID or Password Incorrect'),
(9, 5, 'en', 'Logout Successfully'),
(11, 6, 'en', 'No Record Found'),
(13, 7, 'en', 'Signup Succesfully'),
(15, 8, 'en', 'Phone Number already exist'),
(17, 9, 'en', 'Email already exist'),
(19, 10, 'en', 'rc copy missing'),
(21, 11, 'en', 'License copy missing'),
(23, 12, 'en', 'Insurance copy missing'),
(25, 13, 'en', 'Password Changed'),
(27, 14, 'en', 'Old Password Does Not Matched'),
(29, 15, 'en', 'Invalid coupon code'),
(31, 16, 'en', 'Coupon Apply Successfully'),
(33, 17, 'en', 'User not exist'),
(35, 18, 'en', 'Updated Successfully'),
(37, 19, 'en', 'Phone Number Already Exist'),
(39, 20, 'en', 'Online'),
(41, 21, 'en', 'Offline'),
(43, 22, 'en', 'One Time Pass sent to your phone for Verification'),
(45, 23, 'en', 'Rating Successfully'),
(47, 24, 'en', 'Email Sent Succefully'),
(49, 25, 'en', 'Booking Accepted'),
(51, 26, 'en', 'Your Cab has arrived'),
(53, 27, 'en', 'Ride Cancelled Successfully'),
(55, 28, 'en', 'Your Ride Ended'),
(57, 29, 'en', 'Ride Booked Successfully'),
(59, 30, 'en', 'Ride Rejected Successfully'),
(61, 31, 'en', 'Ride Has Started'),
(63, 32, 'en', 'New Ride Allocated'),
(65, 33, 'en', 'Ride Cancelled By Customer'),
(67, 34, 'en', 'Booking Accepted'),
(69, 35, 'en', 'Booking Rejected'),
(71, 36, 'en', 'Booking Canceled By Driver');

-- --------------------------------------------------------

--
-- Table structure for table `table_normal_ride_rating`
--

CREATE TABLE `table_normal_ride_rating` (
  `rating_id` int(11) NOT NULL,
  `ride_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_rating_star` float NOT NULL,
  `user_comment` text NOT NULL,
  `driver_id` int(11) NOT NULL,
  `driver_rating_star` float NOT NULL,
  `driver_comment` text NOT NULL,
  `rating_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_normal_ride_rating`
--

INSERT INTO `table_normal_ride_rating` (`rating_id`, `ride_id`, `user_id`, `user_rating_star`, `user_comment`, `driver_id`, `driver_rating_star`, `driver_comment`, `rating_date`) VALUES
(1, 1, 1, 4.5, '', 2, 0, '', '2017-10-28'),
(2, 2, 1, 4.5, '', 3, 0, '', '2017-10-30'),
(3, 3, 1, 4.5, '', 3, 0, '', '2017-10-30'),
(4, 7, 1, 4, '', 3, 0, '', '2017-10-31'),
(5, 8, 1, 3.5, '', 3, 0, '', '2017-10-31'),
(6, 10, 5, 5, '', 4, 0, '', '2017-11-02'),
(7, 11, 5, 5, '', 4, 0, '', '2017-11-02'),
(8, 12, 5, 5, '', 4, 0, '', '2017-11-02'),
(9, 13, 1, 4.5, '', 3, 0, '', '2017-11-03'),
(10, 14, 1, 4, '', 3, 0, '', '2017-11-03'),
(11, 15, 1, 4.5, '', 3, 0, '', '2017-11-17'),
(12, 16, 1, 4, '', 3, 0, '', '2017-11-17'),
(13, 17, 1, 3, '', 3, 0, '', '2017-11-17'),
(14, 18, 1, 3.5, '', 3, 0, '', '2017-11-17'),
(15, 19, 1, 4, '', 3, 0, '', '2017-11-17'),
(16, 20, 1, 4.5, '', 3, 0, '', '2017-11-18'),
(17, 21, 1, 3, '', 3, 0, '', '2017-11-18');

-- --------------------------------------------------------

--
-- Table structure for table `table_notifications`
--

CREATE TABLE `table_notifications` (
  `message_id` int(11) NOT NULL,
  `message` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_rental_rating`
--

CREATE TABLE `table_rental_rating` (
  `rating_id` int(11) NOT NULL,
  `rating_star` varchar(255) NOT NULL,
  `rental_booking_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `table_user_rides`
--

CREATE TABLE `table_user_rides` (
  `user_ride_id` int(11) NOT NULL,
  `ride_mode` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL DEFAULT '0',
  `city_id` int(11) NOT NULL,
  `ride_type` int(11) NOT NULL DEFAULT '1',
  `booking_id` int(11) NOT NULL,
  `ride_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user_rides`
--

INSERT INTO `table_user_rides` (`user_ride_id`, `ride_mode`, `user_id`, `driver_id`, `city_id`, `ride_type`, `booking_id`, `ride_date`) VALUES
(1, 1, 1, 2, 0, 1, 1, '0000-00-00'),
(2, 1, 1, 2, 0, 1, 2, '0000-00-00'),
(3, 1, 1, 2, 0, 1, 1, '0000-00-00'),
(4, 1, 1, 0, 0, 1, 2, '0000-00-00'),
(5, 1, 1, 3, 0, 1, 3, '0000-00-00'),
(6, 1, 1, 3, 0, 1, 4, '0000-00-00'),
(7, 1, 1, 3, 0, 1, 5, '0000-00-00'),
(8, 1, 1, 3, 0, 1, 6, '0000-00-00'),
(9, 1, 1, 3, 0, 1, 7, '0000-00-00'),
(10, 1, 1, 3, 0, 1, 8, '0000-00-00'),
(11, 1, 1, 3, 0, 1, 9, '0000-00-00'),
(12, 1, 1, 0, 0, 2, 10, '2017-01-11'),
(13, 1, 5, 4, 0, 1, 11, '0000-00-00'),
(14, 1, 5, 0, 0, 1, 0, '0000-00-00'),
(15, 1, 5, 0, 0, 1, 0, '0000-00-00'),
(16, 1, 5, 0, 0, 1, 0, '0000-00-00'),
(17, 1, 5, 4, 0, 1, 12, '0000-00-00'),
(18, 1, 5, 4, 0, 1, 13, '0000-00-00'),
(19, 1, 5, 4, 0, 1, 14, '0000-00-00'),
(20, 1, 1, 3, 0, 1, 15, '0000-00-00'),
(21, 1, 1, 0, 0, 1, 16, '0000-00-00'),
(22, 1, 1, 3, 0, 1, 17, '0000-00-00'),
(23, 1, 5, 0, 0, 1, 18, '0000-00-00'),
(24, 1, 5, 0, 0, 1, 19, '0000-00-00'),
(25, 1, 5, 4, 0, 1, 20, '0000-00-00'),
(26, 1, 5, 4, 0, 1, 21, '0000-00-00'),
(27, 1, 7, 4, 0, 1, 22, '0000-00-00'),
(28, 1, 5, 4, 0, 1, 23, '0000-00-00'),
(29, 1, 5, 4, 0, 1, 24, '0000-00-00'),
(30, 1, 6, 4, 0, 1, 25, '0000-00-00'),
(31, 1, 1, 3, 0, 1, 26, '0000-00-00'),
(32, 1, 1, 3, 0, 1, 27, '0000-00-00'),
(33, 1, 1, 3, 0, 1, 28, '0000-00-00'),
(34, 1, 1, 3, 0, 1, 29, '0000-00-00'),
(35, 1, 1, 3, 0, 1, 30, '0000-00-00'),
(36, 1, 1, 3, 0, 1, 31, '0000-00-00'),
(37, 1, 1, 3, 0, 1, 32, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL DEFAULT '1',
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `user_location` varchar(255) NOT NULL,
  `device_id` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `wallet_money` varchar(255) NOT NULL DEFAULT '0',
  `register_date` varchar(255) NOT NULL,
  `referral_code` varchar(255) NOT NULL,
  `free_rides` int(11) NOT NULL,
  `referral_code_send` int(11) NOT NULL,
  `phone_verified` int(11) NOT NULL,
  `email_verified` int(11) NOT NULL,
  `password_created` int(11) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `facebook_mail` varchar(255) NOT NULL,
  `facebook_image` varchar(255) NOT NULL,
  `facebook_firstname` varchar(255) NOT NULL,
  `facebook_lastname` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `google_name` varchar(255) NOT NULL,
  `google_mail` varchar(255) NOT NULL,
  `google_image` varchar(255) NOT NULL,
  `google_token` text NOT NULL,
  `facebook_token` text NOT NULL,
  `token_created` int(11) NOT NULL,
  `login_logout` int(11) NOT NULL,
  `rating` varchar(255) NOT NULL,
  `user_delete` int(11) NOT NULL DEFAULT '0',
  `unique_number` varchar(255) NOT NULL,
  `user_signup_type` int(11) NOT NULL DEFAULT '1',
  `user_signup_date` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_type`, `user_name`, `user_email`, `user_phone`, `user_password`, `user_image`, `user_location`, `device_id`, `flag`, `wallet_money`, `register_date`, `referral_code`, `free_rides`, `referral_code_send`, `phone_verified`, `email_verified`, `password_created`, `facebook_id`, `facebook_mail`, `facebook_image`, `facebook_firstname`, `facebook_lastname`, `google_id`, `google_name`, `google_mail`, `google_image`, `google_token`, `facebook_token`, `token_created`, `login_logout`, `rating`, `user_delete`, `unique_number`, `user_signup_type`, `user_signup_date`, `status`) VALUES
(1, 1, 'vishal .', 'vishal@apporio.com', '+918950368358', '123456', 'http://apporio.org/CallTaxee/CallTaxee/uploads/1509692614092.jpg', 'IRIS or 49, Gurugram, Haryana 122018, India', '', 0, '0', 'Friday, Oct 27', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-10-27', 1),
(2, 1, 'fareed wardak .', 'fareedwardak@gmail.com', '+19254782600', 'call2taxi', '', '', '', 0, '0', 'Monday, Oct 30', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-10-30', 1),
(3, 1, 'fareed .', 'fredwardak@outlook.com', '+14084766300', 'ctaxi', '', '', '', 0, '0', 'Wednesday, Nov 1', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-01', 1),
(4, 1, 'shilpa .', 'shilpa@apporio.com', '+918130039030', '123456', '', '', '', 0, '0', 'Wednesday, Nov 1', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '2017-11-01', 1),
(5, 1, 'fareed .', 'fred.wardak@smarttaxiweb.com', '+19254782600', 'cTaxi', '', '', '', 0, '0', 'Wednesday, Nov 1', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-11-01', 1),
(6, 1, 'aman .', 'amanwardak@gmail.com', '+19257846952', 'taxeep2ss', '', '', '', 0, '0', 'Friday, Nov 3', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 1, '', 0, '', 1, '2017-11-03', 1),
(7, 1, 'Fareed', 'fred.wardak@smarttaxiweb.com', '+19254782608', '', '', '', '', 0, '0', '', '', 0, 0, 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '', 0, 0, '', 0, '', 1, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `user_device_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `flag` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `login_logout` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`user_device_id`, `user_id`, `device_id`, `flag`, `unique_id`, `login_logout`) VALUES
(1, 545, 'eLE6sDe39B8:APA91bF7Z1nBzUy2SL9bPymbNc3uyEoSBXsFcqkd0brkZfOq8Qj5Be5avtqj_cRIAPYkCskGPreqFXjKXPPC6azIz7FD4mK3v1NcbQXnCmMnyGWwRu9iU6txYPCjzHXJku9r7yiV1Co4', 0, '6e52b5b177922b25', 1),
(2, 1, 'dOuXJeHOCDM:APA91bG4ZH4tfAJIX_pA3X6LbABf5aUgzQZqErG8ylm8KICld1HnqZio-TJDUypYQZ0QpAthQ8KXZQWoE7M8O88s9ctcgGDVX6N0pyr93ROgU8iu97vUHdX2K6isi2HVB04PzZPuq6qj', 0, 'eff54ff52339245c', 1),
(3, 3, 'dDdX3tqYf3s:APA91bFC07kLgU1BehfkIXewpqSdmf_Vv77Li7bito_9OcBnhlFIe12gfh94GfB6HWB2VBNqwQlV_nsv8CMuMLKxHJmtSar7cd1rIGWFjyS8RObIk-DvPuS_B5g53oUPS71Lc2dHxqCh', 0, '66ca0e0134f78a8f', 1),
(4, 1, 'ceXe9hxksxA:APA91bGNMj40eDVb2S1zKrAnc7hNowWQ6LIoQRIfqJtHRWrs4UAQ9nv0IMpOINey40P-evsJdhoZMeEo6gfsNwmBocBj08MQ7oVW9-dJlTWWeK_78mCGNqxsl-Siyy6Ri4OXZS1BtAMi', 0, 'c06c40037632ed39', 1),
(5, 5, 'e77odqHkr-Q:APA91bFLEkfGiIRVunhK4TC97XZwU5HibAAx_YKMhIgoyCbcrq10ONLbaKRsUwHmHWxYZqNmnfPXcBJz6MbI_DU33AQZABuUbfIEb2SsdnalwX0_VBu_rxR2HOUcKecW_W7619zG5aXb', 0, '172544899efdef7e', 1),
(6, 4, 'dZ5fgYUR-ok:APA91bEOWsjOTNhEk3pmzNcgt_eWUjVlB67o7Ea2Y44LKxohDac-Jg7UrzyaYzk8I9aTwnYVcwvL1vCh-riaU-KkmyPLE0REOX0tm7oWmn-sMuvRkhGS7UWZ_BXJZXX8jDwKsq4WZquk', 0, '9f1891df4b8e4e77', 1),
(7, 6, 'fwXJjQL2EQg:APA91bHOTrsulVHoGKnLDvilueNv8kJG3366lSRzj6Ho4smDRpSkGuaL3Am1dsn-28WLe2gSZyHM8W-71bEG3ZcEphaSwNgjcj34EABgKpbMjdDRR4XSpzOXhYhjQuQ8pz4UcDEaj19D', 0, 'baa2be1f82d3bd75', 1),
(8, 5, 'dG-fxWCn1ok:APA91bGE8hYgCnUqZgGIfPQbTfNC91RRs5XnyJG1BLzRI6hz1vLFYV-Wt_vDWa0fnGTQE1xCkBwhuzSHVjP1afymlcIa3zS9WQX2dl_xqFFwdUPLclQ-rUx4hwG8RUtfBPcQ39zZ1sIb', 0, '63a1ff0e132f97af', 1);

-- --------------------------------------------------------

--
-- Table structure for table `version`
--

CREATE TABLE `version` (
  `version_id` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `application` varchar(255) NOT NULL,
  `name_min_require` varchar(255) NOT NULL,
  `code_min_require` varchar(255) NOT NULL,
  `updated_name` varchar(255) NOT NULL,
  `updated_code` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version`
--

INSERT INTO `version` (`version_id`, `platform`, `application`, `name_min_require`, `code_min_require`, `updated_name`, `updated_code`) VALUES
(1, 'Android', 'Customer', '', '12', '2.6.20170216', '16'),
(2, 'Android', 'Driver', '', '12', '2.6.20170216', '16');

-- --------------------------------------------------------

--
-- Table structure for table `web_about`
--

CREATE TABLE `web_about` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_about`
--

INSERT INTO `web_about` (`id`, `title`, `description`) VALUES
(1, 'About Us', 'Apporio Infolabs Pvt. Ltd. is an ISO certified\nmobile application and web application development company in India. We provide end to end solution from designing to development of the software. We are a team of 30+ people which includes experienced developers and creative designers.\nApporio Infolabs is known for delivering excellent quality software to its clients. Our client base is spreads over more than 20 countries including India, US, UK, Australia, Spain, Norway, Sweden, UAE, Saudi Arabia, Qatar, Singapore, Malaysia, Nigeria, South Africa, Italy, Bermuda and Hong Kong.');

-- --------------------------------------------------------

--
-- Table structure for table `web_contact`
--

CREATE TABLE `web_contact` (
  `id` int(65) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title1` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_contact`
--

INSERT INTO `web_contact` (`id`, `title`, `title1`, `email`, `phone`, `skype`, `address`) VALUES
(1, 'Contact Us', 'Contact Info', 'hello@apporio.com', '+91 8800633884', 'apporio', 'Apporio Infolabs Pvt. Ltd., Gurugram, Haryana, India');

-- --------------------------------------------------------

--
-- Table structure for table `web_driver_signup`
--

CREATE TABLE `web_driver_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_driver_signup`
--

INSERT INTO `web_driver_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

-- --------------------------------------------------------

--
-- Table structure for table `web_home`
--

CREATE TABLE `web_home` (
  `id` int(11) NOT NULL,
  `web_title` varchar(765) DEFAULT NULL,
  `app_name` varchar(255) NOT NULL DEFAULT '',
  `web_footer` varchar(765) DEFAULT NULL,
  `banner_img` varchar(765) DEFAULT NULL,
  `app_heading` varchar(765) DEFAULT NULL,
  `app_heading1` varchar(765) DEFAULT NULL,
  `app_screen1` varchar(765) DEFAULT NULL,
  `app_screen2` varchar(765) DEFAULT NULL,
  `app_details` text,
  `market_places_desc` text,
  `google_play_btn` varchar(765) DEFAULT NULL,
  `google_play_url` varchar(765) DEFAULT NULL,
  `itunes_btn` varchar(765) DEFAULT NULL,
  `itunes_url` varchar(765) DEFAULT NULL,
  `heading1` varchar(765) DEFAULT NULL,
  `heading1_details` text,
  `heading1_img` varchar(765) DEFAULT NULL,
  `heading2` varchar(765) DEFAULT NULL,
  `heading2_img` varchar(765) DEFAULT NULL,
  `heading2_details` text,
  `heading3` varchar(765) DEFAULT NULL,
  `heading3_details` text,
  `heading3_img` varchar(765) DEFAULT NULL,
  `parallax_heading1` varchar(765) DEFAULT NULL,
  `parallax_heading2` varchar(765) DEFAULT NULL,
  `parallax_details` text,
  `parallax_screen` varchar(765) DEFAULT NULL,
  `parallax_bg` varchar(765) DEFAULT NULL,
  `parallax_btn_url` varchar(765) DEFAULT NULL,
  `features1_heading` varchar(765) DEFAULT NULL,
  `features1_desc` text,
  `features1_bg` varchar(765) DEFAULT NULL,
  `features2_heading` varchar(765) DEFAULT NULL,
  `features2_desc` text,
  `features2_bg` varchar(765) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_home`
--

INSERT INTO `web_home` (`id`, `web_title`, `app_name`, `web_footer`, `banner_img`, `app_heading`, `app_heading1`, `app_screen1`, `app_screen2`, `app_details`, `market_places_desc`, `google_play_btn`, `google_play_url`, `itunes_btn`, `itunes_url`, `heading1`, `heading1_details`, `heading1_img`, `heading2`, `heading2_img`, `heading2_details`, `heading3`, `heading3_details`, `heading3_img`, `parallax_heading1`, `parallax_heading2`, `parallax_details`, `parallax_screen`, `parallax_bg`, `parallax_btn_url`, `features1_heading`, `features1_desc`, `features1_bg`, `features2_heading`, `features2_desc`, `features2_bg`) VALUES
(1, 'Apporiotaxi || Website', 'Apporio Taxi', '2017 Apporio Taxi', 'uploads/website/banner_1501227855.jpg', 'MOBILE APP', 'Why Choose Apporio for Taxi Hire', 'uploads/website/heading3_1500287136.png', 'uploads/website/heading3_1500287883.png', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users (both riders and drivers).\r\n\r\nThe work flow of the Apporio taxi starts with Driver making a Sign Up request. Driver can make a Sign Up request using the Driver App. Driver needs to upload his identification proof and vehicle details to submit a Sign Up request.', 'ApporioTaxi on iphone & Android market places', 'uploads/website/google_play_btn1501228522.png', 'https://play.google.com/store/apps/details?id=com.apporio.demotaxiapp', 'uploads/website/itunes_btn1501228522.png', 'https://itunes.apple.com/us/app/apporio-taxi/id1163580825?mt=8', 'Easiest way around', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading1_img1501228907.png', 'Anywhere, anytime', 'uploads/website/heading2_img1501228907.png', ' It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'Low-cost to luxury', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using, Content here, content here, making it look like readable English.', 'uploads/website/heading3_img1501228907.png', 'Why Choose', 'APPORIOTAXI for taxi hire', 'Require an Uber like app for your own venture? Get taxi app from Apporio Infolabs. Apporio Taxi is a taxi booking script which is an Uber Clone for people to buy taxi app for their own business. The complete solution consists of a Rider App, Driver App and an Admin App to manage and monitor the complete activities of app users both riders and drivers.', 'uploads/website/heading3_1500287883.png', 'uploads/website/parallax_bg1501235792.jpg', '', 'Helping cities thrive', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features1_bg1501241213.png', 'Safe rides for everyone', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'uploads/website/features2_bg1501241213.png');

-- --------------------------------------------------------

--
-- Table structure for table `web_home_pages`
--

CREATE TABLE `web_home_pages` (
  `page_id` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `long_content` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  `big_image` varchar(255) NOT NULL DEFAULT '',
  `blog_date` varchar(255) NOT NULL DEFAULT '',
  `blog_time` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `web_home_pages`
--

INSERT INTO `web_home_pages` (`page_id`, `heading`, `content`, `long_content`, `image`, `big_image`, `blog_date`, `blog_time`) VALUES
(1, 'Cabs for Every Pocket', 'From Sedans and SUVs to Luxury cars for special occasions, we have cabs to suit every pocket', '', 'webstatic/img/ola-article/why-ola-1.jpg', '', 'Wednesday, Oct 11', '18:50:20'),
(2, 'Secure and Safer Rides', 'Verified drivers, an emergency alert button, and live ride tracking are some of the features that we have in place to ensure you a safe travel experience.', '', 'webstatic/img/ola-article/why-ola-3.jpg', '', '', ''),
(3, 'Apporio Select', 'A membership program with Apporio that lets you ride a Prime Sedan at Mini fares, book cabs without peak pricing and has zero wait time', '', 'webstatic/img/ola-article/why-ola-2.jpg', '', '', ''),
(4, 'In Cab Entertainment', 'Play music, watch videos and a lot more with Apporio Play! Also stay connected even if you are travelling through poor network areas with our free wifi facility.', '', 'webstatic/img/ola-article/why-ola-9.jpg', '', '', ''),
(5, 'Cashless Rides', 'Now go cashless and travel easy. Simply recharge your Apporio money or add your credit/debit card to enjoy hassle free payments.', '', 'webstatic/img/ola-article/why-ola-5.jpg', '', '', ''),
(6, 'Share and Express', 'To travel with the lowest fares choose Apporio Share. For a faster travel experience we have Share Express on some fixed routes with zero deviations. Choose your ride and zoom away!', '', 'webstatic/img/ola-article/why-ola-3.jpg', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `web_rider_signup`
--

CREATE TABLE `web_rider_signup` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_rider_signup`
--

INSERT INTO `web_rider_signup` (`id`, `title`, `description`) VALUES
(1, 'LOREM IPSUM IS SIMPLY DUMMY TEXT PRINTING', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  ADD PRIMARY KEY (`admin_panel_setting_id`);

--
-- Indexes for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `application_currency`
--
ALTER TABLE `application_currency`
  ADD PRIMARY KEY (`application_currency_id`);

--
-- Indexes for table `application_version`
--
ALTER TABLE `application_version`
  ADD PRIMARY KEY (`application_version_id`);

--
-- Indexes for table `booking-otp`
--
ALTER TABLE `booking-otp`
  ADD PRIMARY KEY (`otp_id`);

--
-- Indexes for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  ADD PRIMARY KEY (`booking_allocated_id`);

--
-- Indexes for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  ADD PRIMARY KEY (`reason_id`);

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`card_id`);

--
-- Indexes for table `car_make`
--
ALTER TABLE `car_make`
  ADD PRIMARY KEY (`make_id`);

--
-- Indexes for table `car_model`
--
ALTER TABLE `car_model`
  ADD PRIMARY KEY (`car_model_id`);

--
-- Indexes for table `car_type`
--
ALTER TABLE `car_type`
  ADD PRIMARY KEY (`car_type_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`coupons_id`);

--
-- Indexes for table `coupon_type`
--
ALTER TABLE `coupon_type`
  ADD PRIMARY KEY (`coupon_type_id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_support`
--
ALTER TABLE `customer_support`
  ADD PRIMARY KEY (`customer_support_id`);

--
-- Indexes for table `done_ride`
--
ALTER TABLE `done_ride`
  ADD PRIMARY KEY (`done_ride_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  ADD PRIMARY KEY (`driver_earning_id`);

--
-- Indexes for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  ADD PRIMARY KEY (`driver_ride_allocated_id`);

--
-- Indexes for table `extra_charges`
--
ALTER TABLE `extra_charges`
  ADD PRIMARY KEY (`extra_charges_id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`fav_id`);

--
-- Indexes for table `favourites_destinations`
--
ALTER TABLE `favourites_destinations`
  ADD PRIMARY KEY (`favourites_destination_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_option`
--
ALTER TABLE `payment_option`
  ADD PRIMARY KEY (`payment_option_id`);

--
-- Indexes for table `price_card`
--
ALTER TABLE `price_card`
  ADD PRIMARY KEY (`price_id`);

--
-- Indexes for table `push_messages`
--
ALTER TABLE `push_messages`
  ADD PRIMARY KEY (`push_id`);

--
-- Indexes for table `rental_booking`
--
ALTER TABLE `rental_booking`
  ADD PRIMARY KEY (`rental_booking_id`);

--
-- Indexes for table `rental_category`
--
ALTER TABLE `rental_category`
  ADD PRIMARY KEY (`rental_category_id`);

--
-- Indexes for table `rental_payment`
--
ALTER TABLE `rental_payment`
  ADD PRIMARY KEY (`rental_payment_id`);

--
-- Indexes for table `rentcard`
--
ALTER TABLE `rentcard`
  ADD PRIMARY KEY (`rentcard_id`);

--
-- Indexes for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  ADD PRIMARY KEY (`allocated_id`);

--
-- Indexes for table `ride_reject`
--
ALTER TABLE `ride_reject`
  ADD PRIMARY KEY (`reject_id`);

--
-- Indexes for table `ride_table`
--
ALTER TABLE `ride_table`
  ADD PRIMARY KEY (`ride_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `sos`
--
ALTER TABLE `sos`
  ADD PRIMARY KEY (`sos_id`);

--
-- Indexes for table `sos_request`
--
ALTER TABLE `sos_request`
  ADD PRIMARY KEY (`sos_request_id`);

--
-- Indexes for table `suppourt`
--
ALTER TABLE `suppourt`
  ADD PRIMARY KEY (`sup_id`);

--
-- Indexes for table `table_documents`
--
ALTER TABLE `table_documents`
  ADD PRIMARY KEY (`document_id`);

--
-- Indexes for table `table_document_list`
--
ALTER TABLE `table_document_list`
  ADD PRIMARY KEY (`city_document_id`);

--
-- Indexes for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  ADD PRIMARY KEY (`done_rental_booking_id`);

--
-- Indexes for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  ADD PRIMARY KEY (`driver_document_id`);

--
-- Indexes for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  ADD PRIMARY KEY (`driver_online_id`);

--
-- Indexes for table `table_languages`
--
ALTER TABLE `table_languages`
  ADD PRIMARY KEY (`language_id`);

--
-- Indexes for table `table_messages`
--
ALTER TABLE `table_messages`
  ADD PRIMARY KEY (`m_id`);

--
-- Indexes for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_notifications`
--
ALTER TABLE `table_notifications`
  ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  ADD PRIMARY KEY (`rating_id`);

--
-- Indexes for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  ADD PRIMARY KEY (`user_ride_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`user_device_id`);

--
-- Indexes for table `version`
--
ALTER TABLE `version`
  ADD PRIMARY KEY (`version_id`);

--
-- Indexes for table `web_about`
--
ALTER TABLE `web_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_contact`
--
ALTER TABLE `web_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home`
--
ALTER TABLE `web_home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `admin_panel_settings`
--
ALTER TABLE `admin_panel_settings`
  MODIFY `admin_panel_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `All_Currencies`
--
ALTER TABLE `All_Currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `application_currency`
--
ALTER TABLE `application_currency`
  MODIFY `application_currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `application_version`
--
ALTER TABLE `application_version`
  MODIFY `application_version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `booking-otp`
--
ALTER TABLE `booking-otp`
  MODIFY `otp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `booking_allocated`
--
ALTER TABLE `booking_allocated`
  MODIFY `booking_allocated_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancel_reasons`
--
ALTER TABLE `cancel_reasons`
  MODIFY `reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `card_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `car_make`
--
ALTER TABLE `car_make`
  MODIFY `make_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `car_model`
--
ALTER TABLE `car_model`
  MODIFY `car_model_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `car_type`
--
ALTER TABLE `car_type`
  MODIFY `car_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(101) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `coupons_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon_type`
--
ALTER TABLE `coupon_type`
  MODIFY `coupon_type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customer_support`
--
ALTER TABLE `customer_support`
  MODIFY `customer_support_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `done_ride`
--
ALTER TABLE `done_ride`
  MODIFY `done_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `driver`
--
ALTER TABLE `driver`
  MODIFY `driver_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `driver_earnings`
--
ALTER TABLE `driver_earnings`
  MODIFY `driver_earning_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `driver_ride_allocated`
--
ALTER TABLE `driver_ride_allocated`
  MODIFY `driver_ride_allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `extra_charges`
--
ALTER TABLE `extra_charges`
  MODIFY `extra_charges_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `fav_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `favourites_destinations`
--
ALTER TABLE `favourites_destinations`
  MODIFY `favourites_destination_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `no_driver_ride_table`
--
ALTER TABLE `no_driver_ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payment_confirm`
--
ALTER TABLE `payment_confirm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `payment_option`
--
ALTER TABLE `payment_option`
  MODIFY `payment_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_card`
--
ALTER TABLE `price_card`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `push_messages`
--
ALTER TABLE `push_messages`
  MODIFY `push_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rental_booking`
--
ALTER TABLE `rental_booking`
  MODIFY `rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rental_category`
--
ALTER TABLE `rental_category`
  MODIFY `rental_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `rental_payment`
--
ALTER TABLE `rental_payment`
  MODIFY `rental_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rentcard`
--
ALTER TABLE `rentcard`
  MODIFY `rentcard_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `ride_allocated`
--
ALTER TABLE `ride_allocated`
  MODIFY `allocated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `ride_reject`
--
ALTER TABLE `ride_reject`
  MODIFY `reject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ride_table`
--
ALTER TABLE `ride_table`
  MODIFY `ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sos`
--
ALTER TABLE `sos`
  MODIFY `sos_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sos_request`
--
ALTER TABLE `sos_request`
  MODIFY `sos_request_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppourt`
--
ALTER TABLE `suppourt`
  MODIFY `sup_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `table_documents`
--
ALTER TABLE `table_documents`
  MODIFY `document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `table_document_list`
--
ALTER TABLE `table_document_list`
  MODIFY `city_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `table_done_rental_booking`
--
ALTER TABLE `table_done_rental_booking`
  MODIFY `done_rental_booking_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_bill`
--
ALTER TABLE `table_driver_bill`
  MODIFY `bill_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_driver_document`
--
ALTER TABLE `table_driver_document`
  MODIFY `driver_document_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `table_driver_online`
--
ALTER TABLE `table_driver_online`
  MODIFY `driver_online_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `table_languages`
--
ALTER TABLE `table_languages`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `table_messages`
--
ALTER TABLE `table_messages`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `table_normal_ride_rating`
--
ALTER TABLE `table_normal_ride_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `table_notifications`
--
ALTER TABLE `table_notifications`
  MODIFY `message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_rental_rating`
--
ALTER TABLE `table_rental_rating`
  MODIFY `rating_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `table_user_rides`
--
ALTER TABLE `table_user_rides`
  MODIFY `user_ride_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `user_device_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `version`
--
ALTER TABLE `version`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `web_about`
--
ALTER TABLE `web_about`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_contact`
--
ALTER TABLE `web_contact`
  MODIFY `id` int(65) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_driver_signup`
--
ALTER TABLE `web_driver_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home`
--
ALTER TABLE `web_home`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `web_home_pages`
--
ALTER TABLE `web_home_pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `web_rider_signup`
--
ALTER TABLE `web_rider_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
